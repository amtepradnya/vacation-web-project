<?php

class UserStatus_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }


   public function get_userStatus()
   {
    $this->db->select("*");
    $this->db->from("userstatus");
    $query = $this->db->get();
    return $query->result();
   }
  
}

 ?>
