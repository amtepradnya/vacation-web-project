<?php

class AbsencesStatus_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }


   public function get_AbsencesStatus()
   {
    $this->db->select("*");
    $this->db->from("absencesstatus");
    $query = $this->db->get();
    return $query->result();
   }
  
}

 ?>
