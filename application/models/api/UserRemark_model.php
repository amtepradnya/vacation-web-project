<?php

class UserRemark_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function insert_userRemark($data = array()){
        return $this->db->insert("userremark", $data);
    }
    public function get_userRemark()
    {
        $this->db->select("*");
        $this->db->from("userremark");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_createdBy($id)
    {   
        $this->db->where("id", $id);
        $this->db->select("createdBy");
        $this->db->from("userremark");
        $query = $this->db->get();
        return $query->result();
    }
    public function update_userRemark($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("userremark", $info);
    }
    public function delete_userRemark($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("userremark");
    }
  
}

 ?>
