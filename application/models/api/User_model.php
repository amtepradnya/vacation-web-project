<?php

class User_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }






   public function checkimageid($user_id)
   {
     $this->db->where("createdBy", $user_id);
    $this->db->from("file");
    $query = $this->db->get();

    return $query->result();

   }
   public function fetch_user()
   {
    $this->db->select("id,email,role,firstName,lastName,status,isApprover,department, vacationDays,token,avtar,created,createdBy,updatedOn,updatedBy");
    $this->db->from("user");
    $query = $this->db->get();

    return $query->result();

   }
   public function checkuser_role($role)
   {
          $this->db->where("id", $role);
          $this->db->select("role");
          $this->db->from("userrole");
         $rolequery = $this->db->get();
     return $rolequery->result();


   }
   public function checkuser_token($token)
   {
           // $newtoken="abc.eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6I";

       // return $token;
        $this->db->select("id,email,role,firstName,lastName,status,isApprover,department, vacationDays,token,avtar,created,createdBy,updatedOn,updatedBy");
          $this->db->where("token", $token);
          $this->db->from("user");
         $query = $this->db->get();
         return $query->result();
    


   }
     public function updateimage($fileId,$userid, $filedata)
   {

         $this->db->where("createdBy", $userid);
         $this->db->where("id", $fileId);
         return $query=  $this->db->update("file", $filedata); 

   }
    
      public function insert_Image($imagedata)
   {

      $this->db->insert('file', $imagedata);

   $insert_id = $this->db->insert_id();
    return $insert_id;  

   }
 public function matchuser($usermail,$userpassword)
 {
   // print_r($tokeninfo);exit();
     $newpass=md5($userpassword); 
     // print_r($newpass);
      $this->db->select("id,email,role,firstName,lastName,status,isApprover,department, vacationDays,token,avtar,created,createdBy,updatedOn,updatedBy");
          $this->db->where("email", $usermail);
          $this->db->where("password", $newpass);
          $this->db->where("status", "3");
          $this->db->from("user");
         $query = $this->db->get();
         return $query->result();
    

         
    

    
 }
  public function insert_token($tokendata,$userid)
{
             $this->db->where("id", $userid);
         return $query=  $this->db->update("user", $tokendata);

}
public function remove_token($token, $id)
    {
        $this->db->where('id', $id) ;  
        return $this->db->update("user", $token);
     }
    public function checkid_exist( $id)
    {
            $this->db->select("id,email,role,firstName,lastName,status,isApprover,department, vacationDays,token,avtar,created,createdBy,updatedOn,updatedBy");

        $this->db->where('id', $id);
        $this->db->from("user");
         $query = $this->db->get();
      
          return $query->result();
     }
   
   
    public function update_data($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("user", $info);
    }
    public function updated_user($id)
    {
      $users_info['employeeId']=$id;

      $this->db->where("id", $id);
      return $this->db->update("user", $users_info);


    }
    public function delete_data($id)
    {
      $this->db->where("id", $id);
      return $this->db->delete("user");

    }
   
  
    public function get_user($user_data)
    {
        return  $this->db->insert('user', $user_data);

   // $insert_id = $this->db->insert_id();
    // return $insert_id;
    }

    // public function insert_token($authtoken, $emp_id)
    // {
    //   $this->db->where('id', $emp_id) ;  
    //   return $this->db->update("employee", $authtoken);


    // }

    // public function remove_token($auth_token, $id)
    // {
    //   $this->db->where('id', $id) ;  
    //   return $this->db->update("employee", $auth_token);


    // }
    // public function get_email()
    // {
    //   $this->db->select("email");
    //   $q = $this->db->get('employee');
    //   return $q->result();

    // }

  
}

 ?>
