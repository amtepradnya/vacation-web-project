<?php

class UserFile_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function insert_userfile($data = array()){
        return $this->db->insert("userfile", $data);
    }
    public function get_userfile()
    {
        $this->db->select("*");
        $this->db->from("userfile");
        $query = $this->db->get();
        return $query->result();
    }
    public function update_userfile($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("userfile", $info);
    }
    public function delete_userfile($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("userfile");
    }
  
}

 ?>
