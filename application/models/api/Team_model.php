<?php

class Team_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }
  public function save_team($team_data)
  {
  	    return $this->db->insert('team', $team_data);

  }
   public function update_team($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("team", $info);
    }
   public function delete_team($id)
    {
      $this->db->where("id", $id);
      return $this->db->delete("team");

    }
  public function get_team()
   {
    $this->db->select("*");
    $this->db->from("team");
    $query = $this->db->get();

    return $query->result();

   }

      public function checkid_exist( $team_Id)
    {
      $this->db->where('id', $team_Id);
      $this->db->from("team");

      $query = $this->db->get();
      
          return $query->result();
     }
}
?>