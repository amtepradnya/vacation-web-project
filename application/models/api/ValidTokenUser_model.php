<?php

class ValidTokenUser_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }

    public function checkuser_role($role,$token)
    {
      $this->db->where("id", $role);
      $this->db->select("role");
      $this->db->from("userrole");
      $rolequery = $this->db->get();
      $outputRole= $rolequery->result();
      $userArray = json_decode(json_encode($outputRole), true);
    
      if($userArray[0]['role'] != "User"){
        $this->db->select("*");
        $this->db->where("role", $role);
        $this->db->where("token", $token);
        $this->db->from("user");
        $rolequery = $this->db->get();
        $result = $rolequery->result_array();
        
        if(count($result) == 1){
          return 'Not User';
        }else{
          return 'Invalid Token';
        }
     }else{
        return 'User';
    }
  }

  public function checking_role($role,$token)
    {
      $this->db->where("id", $role);
      $this->db->select("role");
      $this->db->from("userrole");
      $rolequery = $this->db->get();
      $outputRole= $rolequery->result();
      $userArray = json_decode(json_encode($outputRole), true);
    
      if($userArray[0]['role'] == "Owner"){
        $this->db->select("*");
        $this->db->where("role", $role);
        $this->db->where("token", $token);
        $this->db->from("user");
        $rolequery = $this->db->get();
        $result = $rolequery->result_array();
        
        if(count($result) == 1){
          return 'Owner';
        }else{
          return 'Invalid Token';
        }
        
      }else if($userArray[0]['role'] == "Admin"){
        $this->db->select("*");
        $this->db->where("role", $role);
        $this->db->where("token", $token);
        $this->db->from("user");
        $rolequery = $this->db->get();
        $result = $rolequery->result_array();
        
        if(count($result) == 1){
          return 'Admin';
        }else{
          return 'Invalid Token';
        }

      }else if($userArray[0]['role'] == "HR"){
        $this->db->select("*");
        $this->db->where("role", $role);
        $this->db->where("token", $token);
        $this->db->from("user");
        $rolequery = $this->db->get();
        $result = $rolequery->result_array();
        
        if(count($result) == 1){
          return 'HR';
        }else{
          return 'Invalid Token';
        }
      }else if($userArray[0]['role'] == "User"){
        $this->db->select("*");
        $this->db->where("role", $role);
        $this->db->where("token", $token);
        $this->db->from("user");
        $rolequery = $this->db->get();
        $result = $rolequery->result_array();
        
        if(count($result) == 1){
          return 'User';
        }else{
          return 'Invalid Token';
        }
      }else{
        return 'Invalid User';
      }

 
    }
 
  
}

 ?>
