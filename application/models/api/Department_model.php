<?php

class Department_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }
  public function save_department($department_data)
  {
  	    return $this->db->insert('department', $department_data);

  }
   public function update_department($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("department", $info);
    }
   public function delete_data($id)
    {
      $this->db->where("id", $id);
      return $this->db->delete("department");

    }
  public function get_department()
   {
    $this->db->select("*");
    $this->db->from("department");
    $query = $this->db->get();

    return $query->result();

   }
      public function checkid_exist( $id)
    {
        $this->db->where('id', $id);
        $this->db->from("department");
         $query = $this->db->get();
      
          return $query->result();
     }
   
}
?>