<?php

class DepartmentEmails_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }
  public function insert_DepartmentEmails($departmentemail)
  {
  	    return $this->db->insert('departmentemails', $departmentemail);

  }
  
   public function delete_DepartmentEmails($deptId,$userId)
    {
      $this->db->where('deptId',$deptId);
    	$this->db->where('userId', $userId);
      return $this->db->delete("departmentemails");

    }
  public function get_DepartmentEmails()
   {
    $this->db->select("*");
    $this->db->from("departmentemails");
    $query = $this->db->get();

    return $query->result();

   }

      public function checkid_exist( $dept_Id, $user_Id)
    {
      $this->db->where('userId', $user_Id);
      $this->db->where('deptId',$dept_Id);
          $this->db->from("departmentemails");

      $query = $this->db->get();
      
          return $query->result();
     }
}
?>