<?php

class Absences_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function insert_Absences($data = array()){
        return $this->db->insert("absences", $data);
    }
    public function get_Absences()
    {
        $this->db->select("*");
        $this->db->from("absences");
        $query = $this->db->get();
        return $query->result();
    }
    public function update_Absences($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("absences", $info);
    }
    public function delete_Absences($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("absences");
    }
    public function get_createdBy($id)
    {   
        $this->db->where("id", $id);
        $this->db->select("createdBy,absenceStatus");
        $this->db->from("absences");
        $query = $this->db->get();
        return $query->result();
    }
    public function insert_Image($imagedata)
   {

      $this->db->insert('file', $imagedata);

   $insert_id = $this->db->insert_id();
    return $insert_id;  

   }
  
}

 ?>
