<?php

class UserTeam_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }
  public function save_userteam($team_data)
  {
  	    return $this->db->insert('userteam ', $team_data);

  }
   public function update_userteam($teamid,$userid, $info)
    {
      $this->db->where('userId',$userid);
      $this->db->where('teamId', $teamid);
      return $this->db->update("userteam", $info);
    }
   public function delete_team($user_Id,$team_Id)
    {
    	$this->db->where('userId',$user_Id);
    	$this->db->where('teamId', $team_Id);

     return  $this->db->delete("userteam");
          

    }
  public function get_team()
   {
    $this->db->select("*");
    $this->db->from("userteam");
    $query = $this->db->get();

    return $query->result();

   }
     public function checkid_exist( $team_Id, $user_Id)
    {
      $this->db->where('userId', $user_Id);
      $this->db->where('teamId',$team_Id);
          $this->db->from("userteam");

      $query = $this->db->get();
      
          return $query->result();
     }
}
?>