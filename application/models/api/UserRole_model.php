<?php

class UserRole_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }


   public function get_userRole()
   {
    $this->db->select("*");
    $this->db->from("userrole");
    $query = $this->db->get();
    return $query->result();
   }
  
}

 ?>
