<?php

class UserHoliday_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function insert_userholiday($data = array()){
        return $this->db->insert("userholiday", $data);
    }
    public function get_userholiday()
    {
        $this->db->select("*");
        $this->db->from("userholiday");
        $query = $this->db->get();
        return $query->result();
    }
    public function update_userholiday($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("userholiday", $info);
    }
    public function delete_userholiday($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("userholiday");
    }
    public function get_createdBy($id)
    {   
        $this->db->where("id", $id);
        $this->db->from("userholiday");
        $query = $this->db->get();
        return $query->result();
    }
  
}

 ?>
