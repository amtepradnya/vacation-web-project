<?php

class Location_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function insert_location($data = array()){
        return $this->db->insert("location", $data);
    }
    public function get_location()
    {
        $this->db->select("*");
        $this->db->from("location");
        $query = $this->db->get();
        return $query->result();
    }
    public function update_location($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("location", $info);
    }
    public function delete_location($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("location");
    }
    public function get_createdBy($id)
    {   
        $this->db->where("id", $id);
        $this->db->from("location");
        $query = $this->db->get();
        return $query->result();
    }
  
}

 ?>
