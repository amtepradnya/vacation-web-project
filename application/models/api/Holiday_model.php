<?php

class Holiday_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function insert_holiday($data = array()){
        return $this->db->insert("holiday", $data);
    }
    public function get_holiday()
    {
        $this->db->select("*");
        $this->db->from("holiday");
        $query = $this->db->get();
        return $query->result();
    }
    public function update_holiday($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("holiday", $info);
    }
    public function delete_holiday($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("holiday");
    }
    public function get_createdBy($id)
    {   
        $this->db->where("id", $id);
        $this->db->from("holiday");
        $query = $this->db->get();
        return $query->result();
    }
  
}

 ?>
