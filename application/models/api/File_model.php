<?php

class File_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

  
        public function add_file($file)
   {
    // return $file;
        return $this->db->insert('file', $file);

   }
    public function get_all_file()
    {
        $this->db->select("*");
        $this->db->from("file");
        $query = $this->db->get();
        return $query->result();
    }
   public function check_userfile($id)
   {
        $this->db->where('id', $id);
        $this->db->from("file");
         $query = $this->db->get();
                   return $query->result();

   }
    public function checkid_exist( $id)
    {

        $this->db->where('id', $id);
        $this->db->from("file");
         $query = $this->db->get();
      
          return $query->result();
     }
    public function get_user_file($id)
    {
        $this->db->where("createdBy", $id);
        $this->db->select("*");
        $this->db->from("file");
        $query = $this->db->get();
        return $query->result();
    }
    public function update_file($id, $info)
    {
      $this->db->where("id", $id);
      return $this->db->update("file", $info);
    }
    public function delete_file($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("file");
    }
  
}

 ?>
