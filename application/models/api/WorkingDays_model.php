<?php

class WorkingDays_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }
  public function save_userWorkingDays($usrworkingarr)
  {
  	    return $this->db->insert('userworkingdays', $usrworkingarr);

  }
   // public function update_reason($id, $reason_datainfo)
   //  {

   //    // return $reason_datainfo;
   //    $this->db->where("id", $id);
   //    return $this->db->update("reason", $reason_datainfo);
   //  }

     public function checkid_exist( $workingId, $user_Id)
    {
      $this->db->where('userId', $user_Id);
      $this->db->where('workingId',$workingId);
          $this->db->from("userworkingdays");

      $query = $this->db->get();
      
          return $query->result();
     }
   public function delete_usrWorkingDays($workingid, $usrid)
    {
      $this->db->where("workingId", $workingid);
      $this->db->where("userId", $usrid);
      return $this->db->delete("userworkingdays");

    }
  public function get_workingdays()
   {
    $this->db->select("*");
    $this->db->from("workingdays");
    $query = $this->db->get();

    return $query->result();

   }
   
    public function get_userworkingdays()
   {
    $this->db->select("*");
    $this->db->from("userworkingdays");
    $query = $this->db->get();

    return $query->result();

   }
}
?>