<?php

class Reason_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }
  public function save_reason($reason_data)
  {
  	    return $this->db->insert('reason', $reason_data);

  }
   public function update_reason($id, $reason_datainfo)
    {

      // return $reason_datainfo;
      $this->db->where("id", $id);
      return $this->db->update("reason", $reason_datainfo);
    }
   public function delete_reason($id)
    {
      $this->db->where("id", $id);
      return $this->db->delete("reason");

    }
  public function get_reason()
   {
    $this->db->select("*");
    $this->db->from("reason");
    $query = $this->db->get();

    return $query->result();

   }
   
      public function checkid_exist( $id)
    {
        $this->db->where('id', $id);
        $this->db->from("reason");
         $query = $this->db->get();
      
          return $query->result();
     }
}
?>