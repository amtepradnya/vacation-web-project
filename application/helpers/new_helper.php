<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  function is_start_date_valid($date) {
    $ci = get_instance();

        if (date('Y-m-d', strtotime($date)) == $date) {
            return TRUE;
        } else {
            $ci->form_validation->set_message('is_start_date_valid', 'The {field} must be in format "yyyy-mm-dd"');
            return FALSE;
        }
        
    }

?>