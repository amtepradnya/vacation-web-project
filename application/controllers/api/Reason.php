<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Reason extends REST_Controller {

	 public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/Reason_model"));
        $this->load->model(array("api/user_model"));

            
    }
    public function index_get()
    {
       $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {


    $reason =
     $this->Reason_model->get_reason();
      if($reason)
            {
              $message = [
                  'status' => true,
                  'data' => $reason,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
     }else{


         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
     }
   }else{
$message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

   }

 }
  public function index_put()
{
  $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
   $reason_arr=[];

$config = [
            [
            'field' => 'colorId',
            'label' => 'colorId',
            'rules' => 'integer',
            'errors' => [
                    'required' => 'The colorId field is required',
            ],
            
            ],
            [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'in_list[0,1]', 

            ],
              [
            'field' => 'requiresApproval',
            'label' => 'requiresApproval',
            'rules' => 'in_list[0,1]',
            
            ],
              [
            'field' => 'reducesDays',
            'label' => 'reducesDays',
            'rules' => 'in_list[0,1]',
              ],
              [
            'field' => 'isPublic',
            'label' => 'isPublic',
            'rules' => 'in_list[0,1]',
           
            ],
                [
            'field' => 'sortIndex',
            'label' => 'sortIndex',
            'rules' => 'integer',
           
            ],

            [
            'field' => 'doctorsNote',
            'label' => 'doctorsNote',
            'rules' => 'integer',
           
            ],


            
];
 // $data = json_decode(file_get_contents("php://input"));
 $input_data = json_decode($this->input->raw_input_stream, true);
  
 $this->form_validation->set_data($input_data);
 $this->form_validation->set_rules($config);
if($this->form_validation->run()==FALSE)
 {
 $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);

}else{
          $reason_id=$input_data['id'];
          $idexist= $this->Reason_model->checkid_exist($reason_id);
        if(!empty($idexist))
            {
          if(isset($input_data['colorId']))
           {
          $reason_arr['colorId']=  $input_data['colorId'];


           }
            if(isset($input_data['name']))
           {
          $reason_arr['name']=  $input_data['name'];


           }
          if(isset($input_data['doctorsNote']))
           {
            $reason_arr['doctorsNote']=  $input_data['doctorsNote'];

           }    
           if(isset($input_data['isPublic']))
           {
            $reason_arr['isPublic']=  $input_data['isPublic'];

           }
           if(isset($input_data['reducesDays']))
           {
            $reason_arr['reducesDays']= $input_data['reducesDays'];

           }
           if(isset($input_data['requiresApproval']))
           {
            $reason_arr['requiresApproval']=  $input_data['requiresApproval'];

           }
           if(isset($input_data['sortIndex']))

           {
            $reason_arr['sortIndex']= $input_data['sortIndex'];

           }
           if(isset( $input_data['status']))
            {
             $reason_arr['status']= $input_data['status'];;
            }
     $reason_arr['updatedBy']=$sessionuserid;
    $reason_arr['updateOn']=date('Y-m-d H:i:s');
    $update_reasion= 
    $this->Reason_model->update_reason($reason_id, $reason_arr);
    if($update_reasion){
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are updated successfully'
            ),REST_Controller::HTTP_OK);

          }
          else{
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are not updated'
            ),REST_Controller::HTTP_NOT_FOUND);
          }
    // print_r($update_reasion);exit();
          }else{
        $message = [
                    'status' => false,
                    'message' => "Id not xist",
                         ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);

      }
        }

     
    }else
      {
   $message = [
                    'status' => false,
                    'message' => "your not authorise user to update data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);


      }
    }else{


         $message = [
                    'status' => false,
                    'message' => " unauthorise user ",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

    }
  }else{

    $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
  }

 

}
           
        
public function index_delete()
{
  $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {

        
     $data = json_decode(file_get_contents("php://input"));

     $id = $this->security->xss_clean($data->id);

      $idexist= $this->Reason_model->checkid_exist($id);
        if(!empty($idexist))
            {
   // print_r($id);exit();

  if($this->Reason_model->delete_reason($id)) {
    $this->response(array(
      'status'=>1,
       'message'=>'feilds are deleted successfully'
      ),REST_Controller::HTTP_OK);  
  }
  else{
    $this->response(array(
      'status'=>0,
       'message'=>'failed to delete feilds'
      ),REST_Controller::HTTP_NOT_FOUND); 
  }
}else{

  $message = [
                    'status' => false,
                    'message' => "Id not xist",
                         ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
}

}else{
   $message = [
                    'status' => false,
                    'message' => "your not authorise user to delete data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

}
}else{
   $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
}
}else{
  $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
}
}
    public function index_post()
    {
       // $user_arr= array();
$user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
    header("Access-Control-Allow-Origin: *");
        // $_POST = $this->security->xss_clean($_POST);
        $colorId = $this->security->xss_clean($this->input->post("colorId")); 
        
        $name = $this->security->xss_clean($this->input->post("name")); 
          $this->form_validation->set_rules("colorId", "colorId", "integer|required");
          $this->form_validation->set_rules("name", "name", "required");
          $this->form_validation->set_rules("sortIndex", "sortIndex", "integer");
          $this->form_validation->set_rules("isPublic", "isPublic", "in_list[0,1]");
          $this->form_validation->set_rules("doctorsNote", "doctorsNote", "integer");
          $this->form_validation->set_rules("reducesDays", "reducesDays", "in_list[1,0]");
          $this->form_validation->set_rules("requiresApproval", "requiresApproval", "in_list[1,0]");
          $this->form_validation->set_rules("status", "status", "in_list[1,0]");




        if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else
      {

         $doctorsNote= $this->security->xss_clean($this->input->post("doctorsNote"));
         $isPublic=$this->security->xss_clean($this->input->post("isPublic"));
        $reducesDays=$this->security->xss_clean($this->input->post("reducesDays"));
        $requiresApproval=$this->security->xss_clean($this->input->post("requiresApproval"));
        $sortIndex=$this->security->xss_clean($this->input->post("sortIndex"));
        $status=$this->security->xss_clean($this->input->post("status"));


            $reason_arr['colorId']=  $colorId;
            $reason_arr['name']=  $name;
            $reason_arr['doctorsNote']=  $doctorsNote;
            $reason_arr['isPublic']=  $isPublic;
            $reason_arr['reducesDays']=  $reducesDays;
            $reason_arr['requiresApproval']=  $requiresApproval;
            $reason_arr['sortIndex']=  $sortIndex;
            $reason_arr['status']=  $status;

           

          // $user_info= $this->session->get_userdata('sessiondata');
          // $updatedbyid=$user_info['sessiondata']['user_id'];
          $reason_arr['createdBy']=$sessionuserid;

         $output= $this->Reason_model->save_reason($reason_arr);
            if($output)
            {
              $message = [
                  'status' => true,
                  'data' => $reason_arr,
                    'message' => "Data added successfully",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }

           }
         }else
         {
             $message = [
                    'status' => false,
                    'message' => "your not authorise user to add data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
         }
       }else{

           $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
       }
     }else{

      $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
     }
   }

}
?>