<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class User extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/user_model"));
        $this->load->helper('date');

            

        
        // Load these helper to create JWT tokens
        // $this->load->helper(['jwt', 'authorization']);    
    }
    public function index_get()
    {
      $headers = $this->input->request_headers();
       $user = $this->user_model->fetch_user();

       $user_info= $this->session->get_userdata('usersessiondata');
       if(isset($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {
           $message = [
                  'status' => true,
                  'data' => $user,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);      
                    }
          else{

         $message = [
                  'status' => true,
                   'data' => $checkuserauth,
                    'message' => "user data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);  
                         }

       }else{
           $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
       }
     }else
     {
          $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
     }
      


    }
    public function logout_User_post()
    {  

        $user_info= $this->session->get_userdata('usersessiondata');


       if(isset($user_info['usersessiondata']))
       {


        $user_id=$user_info['usersessiondata']['id'];
        $usertoken=$user_info['usersessiondata']['token'];

        $update_token=array(
            'token'=>null
        );

        $removetoken=$this->user_model->remove_token($update_token, $user_id);
         $destroysession=$this->session->sess_destroy();
      if($removetoken)
      {
        $message = [
                    'status' => true,
                    'message' => "Logout Successfully "
                ];
                $this->response($message, REST_Controller::HTTP_OK); 
       }else{
           $message = [
                    'status' => false,
                    'message' => "failed to logout "
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 

       } 
    

    }
    else{

 $message = [
                    'status' => false,
                    'message' => "failed to logout "
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);  
         }
  }

    public function login_User_post()
{

   $user_info= $this->session->get_userdata('usersessiondata');


       if(!isset($user_info['usersessiondata'])  )
       {

        // echo "data exits" ; exit();
      
     $user_arr= array();
         // echo "abc";
   
        header("Access-Control-Allow-Origin: *");
        // $_POST = $this->security->xss_clean($_POST);
        $email = $this->security->xss_clean($this->input->post("email"));
        $password = $this->security->xss_clean($this->input->post("password")); 
        
        $this->form_validation->set_rules("email", "email", "valid_email|required");
        $this->form_validation->set_rules("password", "password", "required");


         if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else{
         $output= $this->user_model->matchuser($email,$password );
             if (!empty($output) AND $output != FALSE)
         {
                $token_data['id'] = $output[0]->id;
                $token_data['firstName'] = $output[0]->firstName;
                $token_data['email'] = $output[0]->email;
                $token_data['role'] = $output[0]->role;                
                $token_data['lastName'] =  $output[0]->lastName;
                $token_data['status'] =  $output[0]->status;
                $token_data['isApprover'] =  $output[0]->isApprover;
                $token_data['department'] =  $output[0]->department;
                $token_data['vacationDays'] =  $output[0]->vacationDays;
                $token_data['created'] =  $output[0]->created;
                $token_data['createdBy'] =  $output[0]->createdBy;

          // print_r($token_data);exit();

                $user_token = $this->authorization_token->generateToken($token_data);
                $return_data = [
                    'id' => $output[0]->id,
                    'firstName' => $output[0]->firstName,
                    'email' => $output[0]->email,
                    'token' => $user_token,
                    'role' => $output[0]->role,
                    'lastName' => $output[0]->lastName,
                    'status' => $output[0]->status,
                    'isApprover' => $output[0]->isApprover,
                    'department' => $output[0]->department,
                    'vacationDays' => $output[0]->vacationDays,
                    'created' => $output[0]->created,
                    'createdBy' => $output[0]->createdBy


                ];
                 $this->session->set_userdata('usersessiondata', $return_data);
             $store_token=array(
                    'token'=>$user_token
                );
               $token_insert= $this->user_model->insert_token($store_token, $output[0]->id);
           if($token_insert==true )
           {
                $message = [
                    'status' => true,
                    'data' => $return_data,
                    'message' => "User login successful"
                ];
                $this->response($message, REST_Controller::HTTP_OK);           
               }
      
         }else{

          {
                $message = [
                    'status' => false,
                    'message' => "User failed login "
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 
                }
         }

     
      }
      }else{
            $message = [
                    'status' => false,
                    'message' => "You can't login ! To login please clear session & cookies first"
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }
 // }else
 //       {
 //            $message = [
 //                    'status' => false,
 //                    'message' => "failed to login "
 //                ];
 //                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 
 //       }
         


}
    public function index_put()
{
  // echo "prad"; exit();
   
        $user_data=[]; 
       $user_info= $this->session->get_userdata('usersessiondata');
       if(isset($user_info['usersessiondata'])  )
       {
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

          // echo "<pre>";  print_r($user_info); exit();


       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {

        $userrole=$user_info['usersessiondata']['role'];
         $checkrole= $this->user_model->checkuser_role($userrole);
         $role=$checkrole[0]->role;

         if($role != "User")
          {
            echo "nonuser";
                             // echo "<pre>";  print_r($role); exit();


             $input_data = json_decode($this->input->raw_input_stream, true);
          

     $config = [
 
        [
        'field' => 'email',
        'label' => 'email',
        'rules' => 'valid_email|is_unique[user.email]',
        
        ],
        [
        'field' => 'status',
        'label' => 'status',
        'rules' => "integer|in_list[1,2,3,4,5]",
        
        ],
         [
        'field' => 'role',
        'label' => 'role',
        'rules' => "integer|in_list[1,2,3,4]",
        
        ],
         [
        'field' => 'vacationDays',
        'label' => 'vacationDays',
        'rules' => "integer",
        
        ],
         [
        'field' => 'location',
        'label' => 'location',
        'rules' => "integer",
        
        ],
         [
        'field' => 'department',
        'label' => 'department',
        'rules' => "integer",
        
        ],
        [
        'field' => 'isApprover',
        'label' => 'isApprover',
        'rules' => "integer|in_list[1,0]",
        
        ],
        
];

     $this->form_validation->set_data($input_data);

     $this->form_validation->set_rules($config);

     if($this->form_validation->run()==FALSE)
     {
      $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);

      }else{
           $user_id=$input_data['id'];

         $idexist= $this->user_model->checkid_exist($user_id);
           if(!empty($idexist))
          {
        
         if(isset($input_data['firstName'])){
             $user_data['firstName']= $input_data['firstName'];

          }
          if(isset($input_data['lastName'])){
             $user_data['lastName']= $input_data['lastName'];

          }

            if(isset($input_data['password'])){
             $user_data['password']=  md5($input_data['password']);
            }

          if(isset($input_data['email'])){
             $user_data['email']= $input_data['email'];
          }
           if(isset($input_data['role'])){
             $user_data['role']= $input_data['role'];
          }
           if(isset($input_data['status'])){
             $user_data['status']= $input_data['status'];
          }

          if(isset($input_data['vacationDays'])){
             $user_data['vacationDays']= $input_data['vacationDays'];

          }

          if(isset($input_data['department'])){
             $user_data['department']=  $input_data['department'];
          }
          if(isset($input_data['isApprover'])){
             $user_data['isApprover']=$input_data['isApprover'];

          }
            if(isset($input_data['location'])){
             $user_data['location']=$input_data['location'];

          }   
       
          $user_data['updatedBy']=$sessionuserid;
          $user_data['updatedOn']=date('Y-m-d H:i:s');
          // print_r($user_data);exit();
            $output= $this->user_model->update_data($user_id,$user_data);

           if($output)
            {
                $message = [
                    'status' => true,
                    'data' => $user_data,
                    'message' => "Data Updated successfully",
                                   ];
                $this->response($message, REST_Controller::HTTP_OK);
            
  
            }
            else
            {
                $message = [
                    'status' => FALSE,
                    'message' => "Data failed to update",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);         

           }
         
          }else{
            $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 


          }
         }
       }
         else{
             $input_data = json_decode($this->input->raw_input_stream, true);
        $user_id=$input_data['id'];
               $oldfirstname=$user_info['usersessiondata']['firstName'];
               $oldlastname=$user_info['usersessiondata']['lastName'];
             
              // echo "<pre>";  print_r($oldfirst);
               // echo "<pre>";  print_r($oldlast);


              // echo "<pre>";  print_r($input_data['firstName']); exit();

          if(($user_id ==$sessionuserid) && (!isset($input_data['status'])) && (!isset($input_data['role'])) && (!isset($input_data['email'])) && (!isset($input_data['vacationDays'])) &&
          (!isset($input_data['department'])) &&  (!isset($input_data['isApprover'])) &&        (!isset($input_data['location'])) )




        
          {
        $usertoupdate=[]; 

 
             // echo "<pre>";  print_r($input_data); exit();

          if(isset($input_data['firstName'])){
             $usertoupdate['firstName']= $input_data['firstName'];

          }
          if(isset($input_data['lastName'])){
             $usertoupdate['lastName']= $input_data['lastName'];

          }

            // if(isset($input_data['password'])){
            //  $usertoupdate['password']=  md5($input_data['password']);
            // }

             $usertoupdate['updatedBy']=$sessionuserid;
             $usertoupdate['updatedOn']=date('Y-m-d H:i:s');
            $updateinfor= $this->user_model->update_data($sessionuserid,$usertoupdate);
         if($updateinfor)
            {
                $message = [
                    'status' => true,
                    'data' => $usertoupdate,
                    'message' => "Data Updated successfully",
                                   ];
                $this->response($message, REST_Controller::HTTP_OK);
            
  
            }
            else
            {
                $message = [
                    'status' => FALSE,
                    'message' => "Data failed to update",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);         

           }

            

       }else{


           $this->response(array(
             'status'=>0,
             'message' => "Your not authorize to update data",
           ),REST_Controller::HTTP_UNAUTHORIZED); 

       }

 

       }        
  
     }
     else{
           $this->response(array(
             'status'=>0,
             'message' => "Your not authorize to update data",
           ),REST_Controller::HTTP_UNAUTHORIZED); 
        }
}else{

 $this->response(array(
             'status'=>0,
             'message' => " unauthorize user",
           ),REST_Controller::HTTP_UNAUTHORIZED); 
}
   }

public function index_delete()
{
  // echo "hu";exit;
      $input_data = json_decode($this->input->raw_input_stream, true);
       $id=$input_data["id"];



       $user_info= $this->session->get_userdata('usersessiondata');
       if(isset($user_info['usersessiondata'])  )
       {
       $usertoken=$user_info['usersessiondata']['token'];
      $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
                 // print_r($checkuserauth); exit();

         $userrole=$user_info['usersessiondata']['role'];
         $checkrole= $this->user_model->checkuser_role($userrole);
         $role=$checkrole[0]->role;

     if($role != "User")
          {
             
              $idexist= $this->user_model->checkid_exist($id);
            if(!empty($idexist))
          {
          $deletedata=$this->user_model->delete_data($id);

           if($deletedata)
           {
           $message = [
                  'status' => true,
                    'message' => "Data is Deleted successfully",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);      
                    }
          else{

         $message = [
                  'status' => false,
                    'message' => "Failed to delete data",
                         ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);  
                   }
            }else{
$message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 
              
            }

       }
       else{

        $message = [
                  'status' => false,
                    'message' => "Your not authorize to delete data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);  

       }
         

}
else{
           $this->response(array(
             'status'=>0,
             'message' => "Your not authorize to delete data",
           ),REST_Controller::HTTP_UNAUTHORIZED); 
        }
        }else{
     $this->response(array(
             'status'=>0,
             'message' => "unauthorize user",
           ),REST_Controller::HTTP_UNAUTHORIZED); 

        }
      }
    public function index_post()
    {
        $user_arr= array();   

        header("Access-Control-Allow-Origin: *");

       $user_info= $this->session->get_userdata('usersessiondata');
        if(isset($user_info['usersessiondata'])  )
       {
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];
       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {

         $userrole=$user_info['usersessiondata']['role'];
         $checkrole= $this->user_model->checkuser_role($userrole);
         $role=$checkrole[0]->role;
         if($role != "User")
          {  
              
        $email = $this->security->xss_clean($this->input->post("email"));
        $firstName = $this->security->xss_clean($this->input->post("firstName")); 
        $lastName = $this->security->xss_clean($this->input->post("lastName")); 
        $role = $this->security->xss_clean($this->input->post("role")); 
        $status = $this->security->xss_clean($this->input->post("status")); 

        // print_r($password);
        //prad
        $this->form_validation->set_rules("email", "email", "valid_email|required|is_unique[user.email]");
        $this->form_validation->set_rules("firstName", "firstName", "required");
        $this->form_validation->set_rules("lastName", "lastName", "required");
        $this->form_validation->set_rules("location", "location", "integer");
        $this->form_validation->set_rules("isApprover", "isApprover", "integer|in_list[0,1]");
        $this->form_validation->set_rules("department", "department", "integer");
        $this->form_validation->set_rules("vacationDays", "vacationDays", "integer");
        $this->form_validation->set_rules("role", "role", "required|integer|in_list[1,2,3,4]");
        $this->form_validation->set_rules("status", "status", "required|integer|in_list[1,2,3,4,5]");




        if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else{
            $user_arr['email']=  $email;
            $user_arr['firstName']= $firstName;
            $user_arr['lastName']=  $lastName;
            $user_arr['status']=  $status;
            $user_arr['role']=  $role;


         $password= $this->security->xss_clean($this->input->post("password"));

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('avtar'))
                {
                        $error = array('error' => $this->upload->display_errors());

                $returndata = array('status'=>404,'data'=>$error,'message'=>'image upload failed');
                    }
                else
                {
                        $filePath='';
                       
                        $data = array('upload_data' => $this->upload->data());
                        $filePath=$data['upload_data']['full_path'];
                        $file_type=$data['upload_data']['file_type'];

                        $uploadData = array("filepath" => $filePath,"filetype" => $file_type, "createdBy"=>$sessionuserid);

                      $getinsertedid=  $this->user_model->insert_Image($uploadData);
                                  $user_arr['avtar']=  $getinsertedid;


                }

         $location= $this->security->xss_clean($this->input->post("location"));
         $isApprover= $this->security->xss_clean($this->input->post("isApprover"));
         $department= $this->security->xss_clean($this->input->post("department"));
         $vacationDays= $this->security->xss_clean($this->input->post("vacationDays"));
         $user_arr['updatedOn']=date('Y-m-d H:i:s');

                   // echo "<pre>";  print_r($sessionuserid); exit();

            $user_arr['vacationDays']=  $vacationDays;
            $user_arr['department']=  $department;
            $user_arr['role']=  $role;
            $user_arr['isApprover']=  $isApprover;
            $user_arr['status']=  $status;
            $user_arr['location']=  $location;
            $user_arr['password']=  md5($password);
            $user_arr['createdBy']=  $sessionuserid;



            $output= $this->user_model->get_user($user_arr);

     // echo "<pre>";  print_r($output); exit();

            if($output)
            {
              
              unset( $user_arr['password'] );

                $message = [
                    'status' => true,
                    'data' => $user_arr,
                    'message' => "Data added successfully",
                                   ];
                $this->response($message, REST_Controller::HTTP_OK);
  
            }
            else
            {
                $message = [
                    'status' => FALSE,
                    'message' => "Data failed to add",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);         

           }

        }
        }else{
            $message = [
                    'status' => FALSE,
                    'message' => "Your not authorize to add data",
                                   ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
          }
        }
        else{
      
$message = [
                    'status' => FALSE,
                    'message' => "Your not authorize to add data",
                                   ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
                  }

    
  }else{
                $message = [

    'status' => FALSE,
                    'message' => "unauthorize user",
                                   ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

  }
}
}




/* End of file Api.php */