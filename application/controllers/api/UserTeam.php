<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class UserTeam extends REST_Controller {

    public function __construct() {
        parent::__construct();
         $this->load->library('session');
        $this->load->database();
        $this->load->model(array("api/UserTeam_model"));
        $this->load->model(array("api/Team_model"));
        $this->load->model(array("api/user_model"));

        

         
    }

    public function index_post()
    {  
       $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
    
     $team_arr= array();
    header("Access-Control-Allow-Origin: *");
        
      $userId = $this->security->xss_clean($this->input->post("userId")); 
     $teamId = $this->security->xss_clean($this->input->post("teamId")); 
     $status = $this->security->xss_clean($this->input->post("status")); 
     $email = $this->security->xss_clean($this->input->post("email")); 

  
     $this->form_validation->set_rules("userId", "userId", "integer|required");
     $this->form_validation->set_rules("teamId", "teamId", "integer|required");
    $this->form_validation->set_rules("status", "status", "integer|in_list[0,1]");
    $this->form_validation->set_rules("email", "email", "valid_email");

            if ($this->form_validation->run() == FALSE)
            {
                $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else{

             $team_arr['teamId']=  $teamId;
             $team_arr['status']=  $status;
             $team_arr['email']=  $email;
             $team_arr['userId'] = $userId;

        
           // print_r($team_arr);
           $team_arr['createdBy']=$sessionuserid;
     $output= $this->UserTeam_model->save_userteam($team_arr);
         if($output)
            {
              $message = [
                  'status' => true,
                  'data' => $team_arr,
                    'message' => "Data Inserted successfully",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
    // print_r($team_arr);exit();
        }
      }else{

         $message = [
                    'status' => false,
                    'message' => "your not authorise to add data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
      }
    }else{


         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
    }

    }else{
         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
    }
  }
    public function index_get()
    {

       $user_info= $this->session->get_userdata('usersessiondata');
       // print_r($user_info);
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           

         $team =
     $this->UserTeam_model->get_team();
      if($team)
            {
              $message = [
                  'status' => true,
                  'data' => $team,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
          }else{

         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

          }
    }else{

         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
    }

  }
    public function index_delete()
{

  $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
    
     $data = json_decode(file_get_contents("php://input"));

  $teamId = $this->security->xss_clean($data->teamId);
  $userId = $this->security->xss_clean($data->userId);
 $idexist= $this->UserTeam_model->checkid_exist($teamId,$userId);
         // echo "<pre>";  print_r($idexist); 
           if(!empty($idexist))
          {
  $output=$this->UserTeam_model->delete_team($userId,$teamId); 
   if($output)
   {
  
    $this->response(array(
      'status'=>1,
       'message'=>'feilds are deleted successfully'
      ),REST_Controller::HTTP_OK);  
  }
  else{
    $this->response(array(
      'status'=>0,
       'message'=>'failed to delete feilds'
      ),REST_Controller::HTTP_NOT_FOUND); 
  }

}else{
  $message = [

  'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
}
}else{
    $message = [
                    'status' => false,
                    'message' => "your not authorise to delete data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

}
}else{
  $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

}
}else{
  $message = [
  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}

}
// public function index_put()
// {

//   $user_info= $this->session->get_userdata('usersessiondata');
//        if(!empty($user_info['usersessiondata']))
//        {
//        $userrole=$user_info['usersessiondata']['role'];
//        $usertoken=$user_info['usersessiondata']['token'];
//        $sessionuserid=$user_info['usersessiondata']['id'];


//        $checkuserauth= $this->user_model->checkuser_token($usertoken);
//        if(empty(!$checkuserauth))
//        {
           
//         $checkrole= $this->user_model->checkuser_role($userrole);

//        if($checkrole[0]->role != "User")
//        {
//          $config = [
        
//          [
//         'field' => 'teamId',
//         'label' => 'teamId',
//         'rules' => 'integer',
//          ],
//            [
//         'field' => 'userId',
//         'label' => 'userId',
//         'rules' => 'integer',
//          ],
//            [
//         'field' => 'status',
//         'label' => 'status',
//         'rules' => 'integer|in_list[1,0]',
//          ],
//         [
//         'field' => 'email',
//         'label' => 'email',
//         'rules' => 'valid_email',
//          ],
// ];
//  $team_data=[];

//  $input_data = json_decode($this->input->raw_input_stream, true);

//  $this->form_validation->set_data($input_data);
//  $this->form_validation->set_rules($config);

//  if($this->form_validation->run()==FALSE)
//  {
//  $message = array(
//                 'status' => false,
//                 'message' => validation_errors()
//             );

//             $this->response($message, REST_Controller::HTTP_NOT_FOUND);

// }else{

//     $teamId=$input_data['teamId'];
//     $userId=$input_data['userId'];
//      $idexist= $this->UserTeam_model->checkid_exist($teamId,$userId);
//          / if(!empty($idexist))
//           {
//                 if(isset($input_data['email']))
//              {
//                 $team_data['email']=$input_data['email'];
//              }    
//              if(isset($input_data['status']))
//              {
//                 $team_data['status']=$input_data['status'];
//              }              
//               $team_data['updateOn']=date('Y-m-d H:i:s');
//               $team_data['updatedBy']=$sessionuserid;
//               $update_team=$this->UserTeam_model->update_userteam($teamId,$userId, $team_data);
//     if($update_team){
//           $this->response(array(
//             'status'=>1,
//              'message'=>'feilds are updated successfully'
//             ),REST_Controller::HTTP_OK);

//           }
//           else{
//           $this->response(array(
//             'status'=>1,
//              'message'=>'feilds are not updated'
//             ),REST_Controller::HTTP_NOT_FOUND);
//           }

                
//           }else{
//                    $message = [

//                     'status' => FALSE,
//                     'message' => "Id not exist",
//                                    ];
//                 $this->response($message, REST_Controller::HTTP_NOT_FOUND);

//           }

//      }
   
// }else{

//       $message = [
//                     'status' => false,
//                     'message' => "your not authorise to update data",
//                          ];
//                 $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

// }
// }else{
//        $message = [
//   'status' => false,
//                     'message' => "unauthorise user",
//                          ];
//                 $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

// }
// }else{

//     $message = [
//   'status' => false,
//                     'message' => "unauthorise user",
//                          ];
//                 $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
// }

// }
}
