<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Team extends REST_Controller {

	 public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
        $this->load->model(array("api/user_model"));
         $this->load->model(array("api/Team_model"));
        
            
    }

public function index_post()
    {

       $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {

       $team_arr= array();
    header("Access-Control-Allow-Origin: *");
        $teamName = $this->security->xss_clean($this->input->post("teamName")); 
          $this->form_validation->set_rules("teamName", "teamName", "required");
          $this->form_validation->set_rules("status", "status", "integer|in_list[0,1]");

            if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else
      {
         $teamName=$this->input->post('teamName');
          $team_arr['teamName']=  $teamName;
         if(!empty($this->input->post('status')))
           {
            $status=$this->input->post('status');
            $team_arr['status']=  $status;

           }
          
           $team_arr['createdBy']=$sessionuserid;
          
     $output= $this->Team_model->save_team($team_arr);
            if($output)
            {
              $message = [
                  'status' => true,
                  'data' => $team_arr,
                    'message' => "Data added successfully",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }

           }

         }else{

       $message = [
                    'status' => false,
                    'message' => "your not authorise to add data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
         }
       }else{

         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 


       }
   }else{
     $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
   }
    }
    public function index_get()
    {

       $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          
    	 $team =
     $this->Team_model->get_team();
      if($team)
            {
              $message = [
                  'status' => true,
                  'data' => $team,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
    }else{


         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }else{

     $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
  }
}
    public function index_put()
{

  $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
      
     
   $team_data=[];
 // $data = json_decode(file_get_contents("php://input"));
 $config = [
        
         [
        'field' => 'status',
        'label' => 'status',
        'rules' => 'in_list[0,1]',
        
      ],
       
];
 $input_data = json_decode($this->input->raw_input_stream, true);
  

 $this->form_validation->set_data($input_data);
 $this->form_validation->set_rules($config);

 if($this->form_validation->run()==FALSE)
 {
 $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);

}else{

    $team_id=$input_data['id'];

      $idexist= $this->Team_model->checkid_exist($team_id);
         // echo "<pre>";  print_r($idexist); 
           if(!empty($idexist))
          {


    $team_data['teamName']=$input_data['teamName'];

  if(isset($input_data['status']))
  {
    $team_data['status']=$input_data['status'];
  }
     $team_data['updatedBy']=$sessionuserid;


$team_data['updateOn']=date('Y-m-d H:i:s');

  $update_team= 
  $this->Team_model->update_team($team_id, $team_data);
    if($update_team){
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are updated successfully'
            ),REST_Controller::HTTP_OK);

          }
          else{
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are not updated'
            ),REST_Controller::HTTP_NOT_FOUND);
          }
      }else{

           $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }
 
 }

  }else{


         $message = [
                    'status' => false,
                    'message' => "your not authorize user to update data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
     }else{


         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
 
}else{

   $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}
}

public function index_delete()
{
 $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
         

     $data = json_decode(file_get_contents("php://input"));

  $id = $this->security->xss_clean($data->id);
  $idexist= $this->Team_model->checkid_exist($id);
         // echo "<pre>";  print_r($idexist); 
           if(!empty($idexist))
          {
   // print_r($id);exit();

  if($this->Team_model->delete_team($id)) {
    $this->response(array(
      'status'=>1,
       'message'=>'feilds are deleted successfully'
      ),REST_Controller::HTTP_OK);  
  }
  else{
    $this->response(array(
      'status'=>0,
       'message'=>'failed to delete feilds'
      ),REST_Controller::HTTP_NOT_FOUND); 
  }

}else{
          $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);

}
   }else{


         $message = [
                    'status' => false,
                    'message' => "your not authorize user to delete data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
    }else{


         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
          

}
}else{

   $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}
}



}
?>