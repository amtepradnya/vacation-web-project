<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
class UserRemark extends REST_Controller{

  public function __construct(){

    parent::__construct();
   
    //load database
    $this->load->database();
    $this->load->model(array("api/UserRemark_model"));
    $this->load->model(array("api/ValidTokenUser_model"));
  }

    public function index_post()
    {
        $headers = $this->input->request_headers();
        $user_info= $this->session->get_userdata('usersessiondata');
        //check user session 
        if(isset($user_info['usersessiondata']))
        {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          //check valid user,hr,admin and owner 
          if($checkuserauth != 'Invalid Token'){
            $createdBy=$user_info['usersessiondata']['id'];
            $remarkBy = $this->security->xss_clean($this->input->post("remarkBy"));
            $remarkFor = $this->security->xss_clean($this->input->post("remarkFor"));
            $remark = $this->security->xss_clean($this->input->post("remark"));
            $status = $this->security->xss_clean($this->input->post("status")); 

            $this->form_validation->set_rules("remarkBy", "remarkBy", "integer|required");
            $this->form_validation->set_rules("remarkFor", "remarkFor", "integer|required");
            $this->form_validation->set_rules("remark", "remark", "required");
            $this->form_validation->set_rules("status", "status", "in_list[0,1]");
            //check vlidation of data
            if($this->form_validation->run() === FALSE){
              $message = array(
                'status' => false,
                'message' => validation_errors()
              );
              $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }else{
              $userRemark = array(
                "remarkBy" => $remarkBy,
                "remarkFor" => $remarkFor,
                "remark" => $remark,
                "createdBy" => $createdBy,
                "status" => $status
              );
              //insert data 
                if($this->UserRemark_model->insert_userRemark($userRemark))
              {
                  $this->response(array(
                  'status'=>1,
                      'message'=>'user remark feilds are added successfully'
                  ),REST_Controller::HTTP_OK);
              }else{
                  $this->response(array(
                  'status'=>0,
                      'message'=>'data not added'),
                      REST_Controller::HTTP_NOT_FOUND);
              }
            }
          }
          //check invalid token is present 
          else if($checkuserauth == 'Invalid Token'){
            $message = [
              'status' => true,
              'message' => "Invalid Token",
            ];
              $this->response($message, REST_Controller::HTTP_OK); 
          }
       }
       else{
        $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
       }
        
    }

    public function index_get()
    {
      $headers = $this->input->request_headers();
      $user_info= $this->session->get_userdata('usersessiondata');
      //check session data present or not 
      if(!empty($user_info['usersessiondata']))
     {
        $userrole=$user_info['usersessiondata']['role'];
        $usertoken=$user_info['usersessiondata']['token'];
        $checkuserauth= $this->ValidTokenUser_model->checkuser_role($userrole,$usertoken);
          //check valid user,hr,admin and owner 
          if($checkuserauth != 'Invalid Token'){
            $user = $this->UserRemark_model->get_userRemark();
            if($user){
              $message = [
                'status' => true,
                'data' => $user,
                'message' => "All Data",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              $this->response(array(
              'status'=>1,
              'message'=>'fields are not'
              ),REST_Controller::HTTP_NOT_FOUND);
            }              
          }else if($checkuserauth == 'Invalid Token'){
              $message = [
                  'status' => true,
                  'message' => "Invalid Token",
                ];
                $this->response($message, REST_Controller::HTTP_OK); 
          }
     }else{
      $message = [
          'status' => true,
          'message' => "Unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
     }
      
    } 

    public function index_put()
    { 
      $user_info=[];
      $user_id='';
      $config = [
        [
          'field' => 'remarkBy',
          'label' => 'remarkBy',
          'rules' => 'integer',
          'errors' => [
          'required' => 'The remarkBy field must contain an integer',
          ],
        ],
        [
          'field' => 'remarkFor',
          'label' => 'remarkFor',
          'rules' => 'integer',
          'errors' => [
          'required' => 'The remarkFor field must contain an integer',
          ],
        ],
        [
          'field' => 'status',
          'label' => 'status',
          'rules' => 'in_list[0,1]',
          'errors' => [
          'required' => 'The status field is 0 or 1 required',
          ],
        ],
      ]; 
      $input_data = json_decode($this->input->raw_input_stream, true);
      $this->form_validation->set_data($input_data);
      $this->form_validation->set_rules($config);

      if($this->form_validation->run()==FALSE)
      {
        $message = array(
          'status' => false,
          'message' => validation_errors()
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }else{
        $user_id=$input_data['id'];
        if(isset($input_data['remarkBy']))
        {
          $user_info['remarkBy']=($input_data['remarkBy']);
        }
        if(isset($input_data['remarkFor']))
        {
          $user_info['remarkFor']=($input_data['remarkFor']);
        }
        if(isset($input_data['remark']))
        {
          $user_info['remark']=($input_data['remark']);
        }
        if(isset($input_data['status']))
        {
          $user_info['status']=($input_data['status']);
        }
      } 

      $remark_info= $this->session->get_userdata('usersessiondata');
      //check session
      if(isset($remark_info['usersessiondata']))
       {
        $userrole=$remark_info['usersessiondata']['role'];
        $usertoken=$remark_info['usersessiondata']['token'];
        $usersessionId=$remark_info['usersessiondata']['id'];
        
         $checkuserauth= $this->ValidTokenUser_model->checkuser_role($userrole,$usertoken);
         //check hr admin owner or user   
         if($checkuserauth != 'Invalid Token'){
              
            $updatedBy=$remark_info['usersessiondata']['id'];
            $user_info['updatedBy']=$updatedBy;
            $user_info['updateOn']=date('Y-m-d H:i:s');
            //check id is null or not
            if($user_id == null || $user_id ==''){
              $message = [
                'status' => true,
                'message' => "Id not exist",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              $outputID = $this->UserRemark_model->get_createdBy($user_id);
              //check id is present or not in db
              if(count($outputID)== null){
                $message = [
                  'status' => true,
                  'message' => "Id not exist",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
              }else{
                $createdById=(array)$outputID[0];
                //check created by and current id 
                if($createdById['createdBy'] == $usersessionId){
                  $update_reasion=$this->UserRemark_model->update_userRemark($user_id, $user_info);
                  if($update_reasion){
                    $message = [
                      'status' => true,
                      'message' => "UserRemark are updated successfully",
                    ];
                    $this->response($message, REST_Controller::HTTP_OK); 
                
                  }else{
                    $message = [
                      'status' => true,
                      'message' => "feilds are not updated",
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                  }
                }else{
                  $message = [
                      'status' => true,
                      'message' => "unauthorise user",
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                }
              }
            }
          }else if($checkuserauth == 'Invalid Token'){
                $message = [
                    'status' => true,
                    'message' => "Invalid Token",
                  ];
                  $this->response($message, REST_Controller::HTTP_OK); 
            }
       }else{
        $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
       }
  
    }
    public function index_delete()
    {
      $data = json_decode(file_get_contents("php://input"));
      $id = $this->security->xss_clean($data->id);
         
      $remark_info= $this->session->get_userdata('usersessiondata');
      //check user session is present or not
      if(isset($remark_info['usersessiondata']))
       {
        $userrole=$remark_info['usersessiondata']['role'];
        $usertoken=$remark_info['usersessiondata']['token'];
        $usersessionId=$remark_info['usersessiondata']['id'];
        
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        //check id is null or not
        if($id == null || $id ==''){
          $message = [
            'status' => true,
            'message' => "Id not exist",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }else{
          $outputID = $this->UserRemark_model->get_createdBy($id);
          //check id is present or not in db
          if(count($outputID)== null){
            $message = [
              'status' => true,
              'message' => "Id not exist",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
           // echo "DD".$checkuserauth;
            //check hr,user,admin present 
           if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' || $checkuserauth == 'HR'){
           //delete own data 
              if($this->UserRemark_model->delete_userRemark($id)) {
                $this->response(array(
                  'status'=>1,
                  'message'=>'UserRemark are deleted successfully'
                  ),REST_Controller::HTTP_OK);  
              }else{
                $this->response(array(
                  'status'=>0,
                  'message'=>'failed to delete feilds'
                  ),REST_Controller::HTTP_NOT_FOUND); 
              }
           
          }else if($checkuserauth == 'User'){
            $createdById=(array)$outputID[0];
             //check user seeion userid and created by id same or not
            if($createdById['createdBy'] == $usersessionId){
            //delete own data 
              if($this->UserRemark_model->delete_userRemark($id)) {
                $this->response(array(
                  'status'=>1,
                  'message'=>'UserRemark are deleted successfully'
                  ),REST_Controller::HTTP_OK);  
              }else{
                $this->response(array(
                  'status'=>0,
                  'message'=>'failed to delete feilds'
                  ),REST_Controller::HTTP_NOT_FOUND); 
              }
            }else{ 
              $message = [
                'status' => true,
                'message' => "unauthorise user",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }
          }else{
              $message = [
              'status' => true,
              'message' => "unauthorise user",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }
        }
      }

       }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
       }

        
    }
}
 ?>
