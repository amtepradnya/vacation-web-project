<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Login_User extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/user_model"));
        $this->load->helper('date');

            

        
        // Load these helper to create JWT tokens
        // $this->load->helper(['jwt', 'authorization']);    
    }
    public function index_get()
    {
      $user = $this->user_model->fetch_user();
      if($user)
            {
              $message = [
                  'status' => true,
                  'data' => $user,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }

    }
    public function index_put()
{
      $user_data=[]; 


$config = [
        [
        'field' => 'firstName',
        'label' => 'firstName',
        'rules' => 'required',
        'errors' => [
                'required' => 'The firstName field is required',
        ],
      ],
        [
        'field' => 'lastName',
        'label' => 'lastName',
        'rules' => 'required',
        'errors' => [
                'required' => 'The lastName field is required',
        ],
        
        ],
        [
        'field' => 'email',
        'label' => 'email',
        'rules' => 'valid_email|required',
        'errors' => [
                'required' => 'The email field is required',
        ],
        
        ],
];
      $input_data = json_decode($this->input->raw_input_stream, true);
      // print_r($input_data); exit();

 $this->form_validation->set_data($input_data);

 $this->form_validation->set_rules($config);

if($this->form_validation->run()==FALSE)
 {
 $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);

}else{
          $user_id=$input_data['id'];
     
          $user_data['firstName']=$input_data['firstName'];
          $user_data['lastName']=$input_data['lastName'];
          $user_data['email']=$input_data['email'];
      
          if(isset($input_data['vacationDays'])){
             $user_data['vacationDays']=  $input_data['vacationDays'];

          }
          if(isset($input_data['department'])){
             $user_data['department']=  $input_data['department'];

          }
          if(isset($input_data['role'])){
             $user_data['role']=  $input_data['role'];

          }
          if(isset($input_data['isApprover'])){
             $user_data['isApprover']=  $input_data['isApprover'];

          }
          if(isset($input_data['status'])){
             $user_data['status']=  $input_data['status'];

          }
            if(isset($input_data['location'])){
             $user_data['location']=  $input_data['location'];

          }
            if(isset($input_data['avtar'])){
             $user_data['avtar']=  $input_data['avtar'];

          }
            if(isset($input_data['password'])){
             $user_data['password']=  md5($input_data['password']);

          }

          $user_info= $this->session->get_userdata('sessiondata');
          $updatedbyid=$user_info['sessiondata']['user_id'];
          $user_data['updatedBy']=$updatedbyid;
          $user_data['updatedOn']=date('Y-m-d H:i:s');
            $output= $this->user_model->update_data($user_id,$user_data);

           if($output)
            {
                $message = [
                    'status' => true,
                    'data' => $user_data,
                    'message' => "Data Updated successfully",
                                   ];
                $this->response($message, REST_Controller::HTTP_OK);
            
  
            }
            else
            {
                $message = [
                    'status' => FALSE,
                    'message' => "Data failed to update",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);         

           }       
  
     }
}
 
public function index_delete()
{
     $data = json_decode(file_get_contents("php://input"));

  $id = $this->security->xss_clean($data->id);
   // print_r($id);exit();

  if($this->user_model->delete_data($id)) {
    $this->response(array(
      'status'=>1,
       'message'=>'feilds are deleted successfully'
      ),REST_Controller::HTTP_OK);  
  }
  else{
    $this->response(array(
      'status'=>0,
       'message'=>'failed to delete feilds'
      ),REST_Controller::HTTP_NOT_FOUND); 
  }

}

    public function index_post()
    {
        $user_arr= array();
        // echo "abc";
    header("Access-Control-Allow-Origin: *");
        // $_POST = $this->security->xss_clean($_POST);
        $email = $this->security->xss_clean($this->input->post("email"));
        $firstName = $this->security->xss_clean($this->input->post("firstName")); 
        $lastName = $this->security->xss_clean($this->input->post("lastName")); 


        // print_r($password);

        $this->form_validation->set_rules("email", "email", "valid_email|required");
        $this->form_validation->set_rules("firstName", "firstName", "required");
        $this->form_validation->set_rules("lastName", "lastName", "required");
        $this->form_validation->set_rules("avtar", "avtar", "integer");
        $this->form_validation->set_rules("location", "location", "integer");
        $this->form_validation->set_rules("isApprover", "isApprover", "integer");
        $this->form_validation->set_rules("department", "department", "integer");
        $this->form_validation->set_rules("vacationDays", "vacationDays", "integer");
        $this->form_validation->set_rules("role", "role", "integer");
        $this->form_validation->set_rules("status", "status", "integer|in_list[0,1]");



        if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else{
            $user_arr['email']=  $email;
            $user_arr['firstName']= $firstName;
            $user_arr['lastName']=  $lastName;

         $password= $this->security->xss_clean($this->input->post("password"));
         $avtar= $this->security->xss_clean($this->input->post("avtar"));
         $location= $this->security->xss_clean($this->input->post("location"));
         $status= $this->security->xss_clean($this->input->post("status"));
         $isApprover= $this->security->xss_clean($this->input->post("isApprover"));
         $role= $this->security->xss_clean($this->input->post("role"));
         $department= $this->security->xss_clean($this->input->post("department"));
         $vacationDays= $this->security->xss_clean($this->input->post("vacationDays"));
         $user_arr['updatedOn']=date('Y-m-d H:i:s');

            $user_arr['vacationDays']=  $vacationDays;
            $user_arr['department']=  $department;
            $user_arr['role']=  $role;
            $user_arr['isApprover']=  $isApprover;
            $user_arr['status']=  $status;
            $user_arr['location']=  $location;
            $user_arr['avtar']=  $avtar;
            $user_arr['password']=  md5($password);

           $user_token = $this->authorization_token->generateToken($user_arr);
           $user_arr['token']=  $user_token;
            $output= $this->user_model->get_user($user_arr);
            $user_arr['user_id']=  $output;
            $this->session->set_userdata('sessiondata', $user_arr);
            if($output)
            {
             $updateid= $this->user_model->updated_user($output);
              if($updateid){
                $message = [
                    'status' => true,
                    'data' => $user_arr,
                    'message' => "Data Inserted successfully",
                                   ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
  
            }
            else
            {
                $message = [
                    'status' => FALSE,
                    'message' => "Data failed to insert",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);         

           }
            


               

        }

     

    

    }

    


}

/* End of file Api.php */