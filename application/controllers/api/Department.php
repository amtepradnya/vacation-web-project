<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Department extends REST_Controller {

	 public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/Department_model"));
        $this->load->model(array("api/user_model"));

        
            
    }
    public function index_get()
    {
       $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
    $department =
     $this->Department_model->get_department();
      if($department)
            {
              $message = [
                  'status' => true,
                  'data' => $department,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
     }
   else{

  $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

   }
 }
  public function index_put()
{
 $department_data=[];
  $user_info= $this->session->get_userdata('usersessiondata');
       if(isset($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {

$config = [
         [
        'field' => 'status',
        'label' => 'status',
        'rules' => 'integer|in_list[0,1]',
        
      ],
       
];
 $input_data = json_decode($this->input->raw_input_stream, true);

 $this->form_validation->set_data($input_data);
 $this->form_validation->set_rules($config);
   // print_r($input_data['name']); exit();
 if($this->form_validation->run()==FALSE)
 {
 $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);

}else{
    $department_id=$input_data['id'];

   $idexist= $this->Department_model->checkid_exist($department_id);
    if(!empty($idexist))
            {

    $department_data['name']=$input_data['name'];
  
     if(isset($input_data['status']))
    {
    $department_data['status']=$input_data['status'];
    }
    
  // echo "<pre>"; print_r($department_data);exit();

 $user_info= $this->session->get_userdata('usersessiondata');
     $updatedbyid=$user_info['usersessiondata']['id'];
     $department_data['updatedBy']=$updatedbyid;
$department_data['updateOn']=date('Y-m-d H:i:s');

$update_depart= 
$this->Department_model->update_department($department_id, $department_data);
if($update_depart){
      $this->response(array(
        'status'=>1,
         'message'=>'feilds are updated successfully'
        ),REST_Controller::HTTP_OK);

      }
      else{
      $this->response(array(
        'status'=>1,
         'message'=>'feilds are not updated'
        ),REST_Controller::HTTP_NOT_FOUND);
      }

    }else{

         $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 
    }  
}
}else
{
     $message = [
                  'status' => false,
                    'message' => "your not authorize to update data"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}
}else{

  $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}

}else{

   $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}
}

public function index_delete()
{
   $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {

  $data = json_decode(file_get_contents("php://input"));

  $id = $this->security->xss_clean($data->id);
   $idexist= $this->Department_model->checkid_exist($id);
    if(!empty($idexist)){
   // print_r($id);exit();

  if($this->Department_model->delete_data($id)) {
    $this->response(array(
      'status'=>1,
       'message'=>'feilds are deleted successfully'
      ),REST_Controller::HTTP_OK);  
  }
  else{
    $this->response(array(
      'status'=>0,
       'message'=>'failed to delete feilds'
      ),REST_Controller::HTTP_NOT_FOUND); 
  }
}else{

  $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 
}
}else{
$message = [
           'status' => false,
                    'message' => "your not authorize to delete data"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
}

}else{

$message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
              }

}else{
  $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
}
}
    public function index_post()
    {
       $department_arr= array();
       $user_arr= array();
     $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {

    header("Access-Control-Allow-Origin: *");
        // $_POST = $this->security->xss_clean($_POST);
          $name = $this->security->xss_clean($this->input->post("name")); 
          
          $this->form_validation->set_rules("name", "name", "required");
          $this->form_validation->set_rules("status", "status", "integer|in_list[0,1]");

            if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else
      {
         $status=$this->security->xss_clean($this->input->post("status")); 
         $department_arr['name']=  $name;
         $department_arr['status']=  $status;

           
          
           $user_info= $this->session->get_userdata('usersessiondata');
           $updatedbyid=$user_info['usersessiondata']['id'];
           $department_arr['createdBy']=$updatedbyid;
          
     $output= $this->Department_model->save_department($department_arr);
            if($output)
            {
              $message = [
                  'status' => true,
                  'data' => $department_arr,
                    'message' => "Data added successfully",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }

           }

         }else{
             $message = [
           'status' => false,
                    'message' => "your not authorize to add data"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

         }
       }else
       {
        $message = [
          'status' => false,
                    'message' => " unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

       }
   
    }else{
        $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }

}
?>