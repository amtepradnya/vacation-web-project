<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class UserHoliday extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/UserHoliday_model"));
    $this->load->model(array("api/ValidTokenUser_model"));
  }
  
    public function index_post()
    {
      $headers = $this->input->request_headers();   
      $UserHolidays_info= $this->session->get_userdata('usersessiondata');
      
      //check user seesion present or not
      if(isset($UserHolidays_info['usersessiondata']))
      {
        $createdBy=$UserHolidays_info['usersessiondata']['id'];
        $userrole=$UserHolidays_info['usersessiondata']['role'];
        $usertoken=$UserHolidays_info['usersessiondata']['token'];
        $createdBy=$UserHolidays_info['usersessiondata']['id'];
        
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
         //check owner,admin,hr and user
        if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' || $checkuserauth == 'HR'){
          $name = $this->security->xss_clean($this->input->post("name"));
        $userHoliday = $this->security->xss_clean($this->input->post("userHoliday"));
        $status = $this->security->xss_clean($this->input->post("status")); 

        $this->form_validation->set_rules("name", "name", "required");
        $this->form_validation->set_rules("userHoliday", "userHoliday", "max_length[10]|is_start_date_valid|trim|required");
        $this->form_validation->set_rules("status", "status", "in_list[0,1]");
        
        if($this->form_validation->run() === FALSE){
          $message = array(
            'status' => false,
            'message' => validation_errors()
          );
          $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else{
            $UserHolidayDetails = array(
                "name" => $name,
                "userHoliday"=>$userHoliday,
                "createdBy" => $createdBy,
                "status" => $status
            );

            if($this->UserHoliday_model->insert_userholiday($UserHolidayDetails))
            {
                $this->response(array(
                'status'=>1,
                    'message'=>'UserHoliday feilds are added successfully'
                ),REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                'status'=>0,
                    'message'=>'data not added'),
                    REST_Controller::HTTP_NOT_FOUND);
            }
          }
        }else{
          $message = [
            'status' => false,
              'message' => "unauthorise user",
                   ];
          $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
         }
      }else{
        $message = [
          'status' => false,
            'message' => "unauthorise user",
                 ];
        $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
      }
    }

    public function index_get()
    {
        
        $user_info= $this->session->get_userdata('usersessiondata');
        //check user session present or not
        if(isset($user_info['usersessiondata']))
        {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
         
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          //check owner,admin,hr,user
          if($checkuserauth != 'Invalid Token' || $checkuserauth != 'Invalid User'){
            //get data 
            $UserHolidays = $this->UserHoliday_model->get_UserHoliday();
            if($UserHolidays){
              $message = [
                'status' => true,
                'data' => $UserHolidays,
                'message' => "All Data",
              ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $this->response(array(
            'status'=>1,
            'message'=>'fields are not present'
            ),REST_Controller::HTTP_NOT_FOUND);
          }
          }else{
            $message = [
              'status' => true,
              'message' => "unauthorise user",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
    } 

    public function index_put()
    {
      $UserHolidays_info=[];
      $user_id='';
      $config = [
        [
          'field' => 'name',
          'label' => 'name',
          'errors' => [
          'required' => 'The name field is required',
          ],
        ],
        [
            'field' => 'userHoliday',
            'label' => 'userHoliday',
            'rules' => 'max_length[10]|is_start_date_valid|trim',
            'errors' => [
            'required' => 'The userHoliday field is required',
            ],
          ],
        [
          'field' => 'status',
          'label' => 'status',
          'rules' => 'in_list[0,1]',
          'errors' => [
          'required' => 'The status field is 0 or 1 required',
          ],
        ],
      ];   
      $input_data = json_decode($this->input->raw_input_stream, true);
      $this->form_validation->set_data($input_data);
      $this->form_validation->set_rules($config);

      if($this->form_validation->run()==FALSE)
      {
        $message = array(
          'status' => false,
          'message' => validation_errors()
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }else{
        $user_id=$input_data['id'];
        if(isset($input_data['name']))
        {
          $UserHolidays_info['name']=($input_data['name']);
        }
        if(isset($input_data['userHoliday']))
        {
          $UserHolidays_info['userHoliday']=($input_data['userHoliday']);
        }
        if(isset($input_data['status']))
        {
          $UserHolidays_info['status']=($input_data['status']);
        }
        $user_info= $this->session->get_userdata('sessiondata');
        //check session data present or not 
        if(isset($user_info['usersessiondata']))
        {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
          $updatedBy=$user_info['usersessiondata']['id'];
          $UserHolidays_info['updatedBy']=$updatedBy;
          
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          //check id is null or empty
          if($user_id == null || $user_id ==''){
            $message = [
              'status' => true,
              'message' => "Please enter id",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $outputID = $this->UserHoliday_model->get_createdBy($user_id);
            //check id is present or not in db
            if(count($outputID)== null){
              $message = [
                'status' => true,
                'message' => "Id not present",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              //check user role 
              if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' ||$checkuserauth == 'HR' ){
              $UserHolidays_info['updateOn']=date('Y-m-d H:i:s');
              //update in db
              $update_reasion=$this->UserHoliday_model->update_UserHoliday($user_id, $UserHolidays_info);
              if($update_reasion){
              $this->response(array(
                'status'=>1,
                'message'=>'UserHoliday are updated successfully'
                ),REST_Controller::HTTP_OK);
              }else{
              $this->response(array(
                'status'=>1,
                'message'=>'feilds are not updated'
                ),REST_Controller::HTTP_NOT_FOUND);
              }
          }else{
            $message = [
              'status' => true,
              'message' => "unauthorise user",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }  
            }

          }
          
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);  
        }
          
      }
       
    }
    public function index_delete()
    {
        $data = json_decode(file_get_contents("php://input"));
        $id = $this->security->xss_clean($data->id);
        
        $user_info= $this->session->get_userdata('usersessiondata');
         //check session
        if(isset($user_info['usersessiondata']))
        {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
          $usersessionId=$user_info['usersessiondata']['id'];
         
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          //check user role 
          if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' ||$checkuserauth == 'HR' ){
            //check id is null or empty
            if($id == null || $id ==''){
              $message = [
                'status' => true,
                'message' => "Please enter id",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              $outputID = $this->UserHoliday_model->get_createdBy($id);
              //check id is present in db
              if(count($outputID)== null){
                $message = [
                  'status' => true,
                  'message' => "Id not present",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
              }else{
                //delete id
                if($this->UserHoliday_model->delete_UserHoliday($id)) {
                  $this->response(array(
                    'status'=>1,
                     'message'=>'UserHoliday are deleted successfully'
                    ),REST_Controller::HTTP_OK);  
                }
                else{
                  $this->response(array(
                    'status'=>0,
                     'message'=>'failed to delete feilds'
                    ),REST_Controller::HTTP_NOT_FOUND); 
                }
              }
            }
          }else{
            $message = [
              'status' => true,
              'message' => "unauthorise user",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
         }
       
    }
}
 ?>
