<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Demo extends REST_Controller {

	 public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/ValidTokenUser_model"));
        
            
    }
    public function index_get()
    {
        $headers = $this->input->request_headers();
        $user_info= $this->session->get_userdata('usersessiondata');
        if(!empty($user_info['usersessiondata']))
       {
            $userrole=$user_info['usersessiondata']['role'];
            $usertoken=$user_info['usersessiondata']['token'];
            $checkuserauth= $this->ValidTokenUser_model->checkuser_role($userrole,$usertoken);
            if($checkuserauth != 'User'){
                $message = [
                    'status' => true,
                    'message' => "All Data Display",
                  ];
                  $this->response($message, REST_Controller::HTTP_OK); 
            }else if($checkuserauth == 'Invalid Token'){
                $message = [
                    'status' => true,
                    'message' => "Invalid Token",
                  ];
                  $this->response($message, REST_Controller::HTTP_OK); 
            }else{
                $message = [
                    'status' => true,
                    'message' => "Particular User Data",
                  ];
                  $this->response($message, REST_Controller::HTTP_OK); 
            }
       }else{
        $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
       }
    }
 
}
?>