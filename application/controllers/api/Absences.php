<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Absences extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/Absences_model"));
    $this->load->model(array("api/user_model"));
    $this->load->model(array("api/ValidTokenUser_model"));
  }
  public function index_post()
  {
      $headers = $this->input->request_headers();   
      $Absences_info= $this->session->get_userdata('usersessiondata');
      $user_arr= array();
      $getinsertedid='';
      //check user seesion present or not
      if(isset($Absences_info['usersessiondata']))
      {
        $createdBy=$Absences_info['usersessiondata']['id'];
        $userrole=$Absences_info['usersessiondata']['role'];
        $usertoken=$Absences_info['usersessiondata']['token'];
        $usersessionId=$Absences_info['usersessiondata']['id'];
        
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        //check owner,admin,hr and user
        if($checkuserauth != 'Invalid User' || $checkuserauth != 'Invalid Token'){
          $reasonId = $this->security->xss_clean($this->input->post("reasonId"));
          $assignedToId = $this->security->xss_clean($this->input->post("assignedToId"));
          $approverId = $this->security->xss_clean($this->input->post("approverId"));
          $start = $this->security->xss_clean($this->input->post("start"));
          $end = $this->security->xss_clean($this->input->post("end"));
          $commentary = $this->security->xss_clean($this->input->post("commentary")); 
          $daysCount = $this->security->xss_clean($this->input->post("daysCount")); 
          $denyReason = $this->security->xss_clean($this->input->post("denyReason")); 
          $absenceStatus = $this->security->xss_clean($this->input->post("absenceStatus")); 
          $doctorsNote = $this->security->xss_clean($this->input->post("doctorsNote")); 
          $substituteId = $this->security->xss_clean($this->input->post("substituteId"));
          $startType = $this->security->xss_clean($this->input->post("startType"));
          $endType = $this->security->xss_clean($this->input->post("endType"));
          $status = $this->security->xss_clean($this->input->post("status"));

          $this->form_validation->set_rules("reasonId", "reasonId", "integer|required");
          $this->form_validation->set_rules("assignedToId", "assignedToId", "integer|required");
          $this->form_validation->set_rules("approverId", "approverId", "integer|required");
          $this->form_validation->set_rules("start", "start", "max_length[10]|is_start_date_valid|trim|required");
          $this->form_validation->set_rules("end", "end", "max_length[10]|is_start_date_valid|trim|required");
          $this->form_validation->set_rules("commentary", "commentary", "required");
          $this->form_validation->set_rules("absenceStatus", "absenceStatus", "in_list[1,2,3]");
          $this->form_validation->set_rules("doctorsNote", "doctorsNote", "file|jpg|png");
          $this->form_validation->set_rules("substituteId", "substituteId", "integer");
          $this->form_validation->set_rules("startType", "startType", "integer");
          $this->form_validation->set_rules("endType", "endType", "integer");
          $this->form_validation->set_rules("status", "status", "in_list[0,1]");


          
          //file upload
          $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

          $this->load->library('upload', $config);

          if ( ! $this->upload->do_upload('doctorsNote'))
          {
            $error = array('error' => $this->upload->display_errors());
            $returndata = array('status'=>404,'data'=>$error,'message'=>'image upload failed');
          }
          else
          {
                  $filePath='';
                  
                  $data = array('upload_data' => $this->upload->data());
                  $filePath=$data['upload_data']['full_path'];
                  $file_type=$data['upload_data']['file_type'];

                  $uploadData = array("filepath" => $filePath,"filetype" => $file_type, "createdBy" => $usersessionId);
                $getinsertedid=  $this->Absences_model->insert_Image($uploadData);
          }

          if($this->form_validation->run() === FALSE){
            $message = array(
              'status' => false,
              'message' => validation_errors()
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
          }else{

            if($start >= $end)
           {
            echo "end date should be greter than and equal to  start date";
            return false;
           }
           
            $AbsencesData = array(
              "reasonId" => $reasonId,
              "assignedToId" => $assignedToId,
              "approverId" => $approverId,
              "start" => $start,
              "end" => $end,
              "commentary" => $commentary,
              "daysCount" => $daysCount,
              "denyReason" => $denyReason,
              "absenceStatus" => 1,
              "doctorsNote" => $getinsertedid,
              "substituteId" => $substituteId,
              "startType" => $startType,
              "endType" => $endType,
              "createdBy" => $createdBy,
              "status" => $status
            );
            //insert data
            if($this->Absences_model->insert_Absences($AbsencesData))
            {
                $this->response(array(
                'status'=>1,
                    'message'=>'Absences feilds are added successfully'
                ),REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                'status'=>0,
                'message'=>'data not added'),
                REST_Controller::HTTP_NOT_FOUND);
            }
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }

  }

    public function index_get()
    {
      $user_info= $this->session->get_userdata('usersessiondata');
      //check user session present or not
      if(isset($user_info['usersessiondata']))
      {
        $userrole=$user_info['usersessiondata']['role'];
        $usertoken=$user_info['usersessiondata']['token'];
      
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        //check owner,admin,hr,user
        if($checkuserauth != 'Invalid Token' || $checkuserauth != 'Invalid User'){
          //get data 
          $AbsencesData = $this->Absences_model->get_Absences();
          if($AbsencesData){
            $message = [
              'status' => true,
              'data' => $AbsencesData,
              'message' => "All Data",
            ];
          $this->response($message, REST_Controller::HTTP_OK);
        }else{
          $this->response(array(
          'status'=>1,
          'message'=>'fields are not present'
          ),REST_Controller::HTTP_NOT_FOUND);
        }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }

      
    } 

    public function userAbsence_put()
  {
    $Absences_info=[];
    $Absences_id='';
    $user_info= $this->session->get_userdata('usersessiondata');
    $config = [
      [
        'field' => 'reasonId',
        'label' => 'reasonId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The reasonId field must contain an integer',
        ],
      ],
      [
        'field' => 'assignedToId',
        'label' => 'assignedToId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The assignedToId field must contain an integer',
        ],
      ],
      [
        'field' => 'approverId',
        'label' => 'approverId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The approverId field must contain an integer',
        ],
      ],
      [
        'field' => 'start',
        'label' => 'start',
        'rules' => 'max_length[10]|is_start_date_valid|trim',
        'errors' => [
        'required' => 'The start field must contain an date',
        ],
      ],
      [
        'field' => 'end',
        'label' => 'end',
        'rules' => 'max_length[10]|is_start_date_valid|trim',
        'errors' => [
        'required' => 'The end field must contain an date',
        ],
      ], 
      [
        'field' => 'commentary',
        'label' => 'commentary',
        'rules' => 'min_length[3]',
        'errors' => [
        'required' => 'The commentary field is required',
        ],
      ],
      [
        'field' => 'absenceStatus',
        'label' => 'absenceStatus',
        'rules' => 'in_list[1]',
        'errors' => [
        'required' => 'The absenceStatus field must contain 1',
        ],
      ],
      [
        'field' => 'doctorsNote',
        'label' => 'doctorsNote',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The doctorsNote field must contain an integer',
        ],
      ],     
      [
        'field' => 'substituteId',
        'label' => 'substituteId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The substituteId field must contain an integer',
        ],
      ],
      [
        'field' => 'startType',
        'label' => 'startType',
        'rules' => 'in_list[0,1]',
        'errors' => [
        'required' => 'The startType field is 0 or 1 required',
        ],
      ],
      [
        'field' => 'endType',
        'label' => 'endType',
        'rules' => 'in_list[0,1]',
        'errors' => [
        'required' => 'The endType field is 0 or 1 required',
        ],
      ],
      [
        'field' => 'status',
        'label' => 'status',
        'rules' => 'in_list[0,1]',
        'errors' => [
        'required' => 'The status field is 0 or 1 required',
        ],
      ]
    ];
    $input_data = json_decode($this->input->raw_input_stream, true);
    $this->form_validation->set_data($input_data);
    $this->form_validation->set_rules($config);

    if($this->form_validation->run()==FALSE)
    {
      $message = array(
        'status' => false,
        'message' => validation_errors()
      );
      $this->response($message, REST_Controller::HTTP_NOT_FOUND);
    }else{

      $user_id=$input_data['id'];
      if(isset($input_data['reasonId']))
      {
        $Absences_info['reasonId']=($input_data['reasonId']);
      }
      if(isset($input_data['assignedToId']))
      {
        $Absences_info['assignedToId']=($input_data['assignedToId']);
      }
      if(isset($input_data['approverId']))
      {
        $Absences_info['approverId']=($input_data['approverId']);
      }
      if(isset($input_data['start']) || isset($input_data['end']))
      {
        //ppp
           if($input_data['start'] >= $input_data['end'])
           {
            echo "end date should be greter than and equal to  start date";
            return false;
           }
        $Absences_info['start']=($input_data['start']);
      
        $Absences_info['end']=($input_data['end']);
      }
      if(isset($input_data['commentary']))
      {
        $Absences_info['commentary']=($input_data['commentary']);
      }
      if(isset($input_data['denyReason']))
      {
        $Absences_info['denyReason']=($input_data['denyReason']);
      }
      if(isset($input_data['absenceStatus']))
      {
        $Absences_info['absenceStatus']=($input_data['absenceStatus']);
      }
      if(isset($input_data['doctorsNote']))
      {
        $Absences_info['doctorsNote']=($input_data['doctorsNote']);
      }
      if(isset($input_data['substituteId']))
      {
        $Absences_info['substituteId']=($input_data['substituteId']);
      }
      if(isset($input_data['startType']))
      {
        $Absences_info['startType']=($input_data['startType']);
      }
      if(isset($input_data['endType']))
      {
        $Absences_info['endType']=($input_data['endType']);
      }
      if(isset($input_data['status']))
      {
        $Absences_info['status']=($input_data['status']);
      }
      
      $user_info= $this->session->get_userdata('sessiondata');
        //check session data present or not 
        if(isset($user_info['usersessiondata']))
        {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
          $updatedBy=$user_info['usersessiondata']['id'];
          $Absences_info['updatedBy']=$updatedBy;
          $Absences_info['updateOn']=date('Y-m-d H:i:s');
          
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);

             // echo "<pre>";  print_r($checkuserauth); exit();

          //check id is null or empty
          if($user_id == null || $user_id ==''){
            $message = [
              'status' => true,
              'message' => "Please enter id",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $outputID = $this->Absences_model->get_createdBy($user_id);
            //check id is present or not in db
            if(count($outputID)== null){
              $message = [
                'status' => true,
                'message' => "Id not present",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              
                //check user role 
              if($checkuserauth == 'User'){
               $createdById=(array)$outputID[0];
                
            //check created by and current id
              if($createdById['createdBy'] == $updatedBy && $createdById['absenceStatus'] == '1'){
                $update_reasion= $this->Absences_model->update_Absences($user_id, $Absences_info);
                if($update_reasion){
                $this->response(array(
                  'status'=>1,
                  'message'=>'Absences are updated successfully'
                  ),REST_Controller::HTTP_OK);
                }
                else{
                $this->response(array(
                  'status'=>1,
                  'message'=>'feilds are not updated'
                  ),REST_Controller::HTTP_NOT_FOUND);
                }
              }else{
                $message = [
                  'status' => false,
                  'message' => "your  not authorize to update data"
                ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
              }

                
              }else{
                $message = [
                  'status' => false,
                  'message' => "your kk not authorize to update data"
                ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
              }
            }
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise 1 user",
          ];
          $this->response($message, REST_Controller::HTTP_OK); 
        }
    }
  }
   
    public function index_delete()
    {
      $data = json_decode(file_get_contents("php://input"));
      $id = $this->security->xss_clean($data->id);
      $user_info= $this->session->get_userdata('usersessiondata');
      //check session
      if(isset($user_info['usersessiondata']))
      {
        $userrole=$user_info['usersessiondata']['role'];
        $usertoken=$user_info['usersessiondata']['token'];
        $usersessionId=$user_info['usersessiondata']['id'];
        
       $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
       if($id == null || $id ==''){
        $message = [
          'status' => true,
          'message' => "Please enter id",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }else{
        $outputID = $this->Absences_model->get_createdBy($id);
        if(count($outputID)== null){
          $message = [
            'status' => true,
            'message' => "Id not present",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }else{
          //check user role
        if($checkuserauth != 'Invalid Token' || $checkuserauth != 'Invalid Token'){
          $createdById=(array)$outputID[0];
          //check created by and current id 
          if($createdById['createdBy'] == $usersessionId && $createdById['absenceStatus'] == '1'){
            if($this->Absences_model->delete_Absences($id)) {
              $this->response(array(
                'status'=>1,
                 'message'=>'Absences are deleted successfully'
                ),REST_Controller::HTTP_OK);  
            }
            else{
              $this->response(array(
                'status'=>0,
                 'message'=>'failed to delete feilds'
                ),REST_Controller::HTTP_NOT_FOUND); 
            }
          }else{
            $message = [
              'status' => true,
              'message' => "unauthorise user",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }  
      }
    }
   }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }   
        
    }
//approve absence or decline
  public function absenceapprove_put()
  {
    $absence_info=[];
    $user_id='';
    $config = [
      [
        'field' => 'reasonId',
        'label' => 'reasonId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The reasonId field must contain an integer',
        ],
      ],
      [
        'field' => 'assignedToId',
        'label' => 'assignedToId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The assignedToId field must contain an integer',
        ],
      ],
      [
        'field' => 'approverId',
        'label' => 'approverId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The approverId field must contain an integer',
        ],
      ],
      [
        'field' => 'start',
        'label' => 'start',
        'rules' => 'max_length[10]|is_start_date_valid|trim',
        'errors' => [
        'required' => 'The start field must contain an date',
        ],
      ],
      [
        'field' => 'end',
        'label' => 'end',
        'rules' => 'max_length[10]|is_start_date_valid|trim',
        'errors' => [
        'required' => 'The end field must contain an date',
        ],
      ], 
      [
        'field' => 'commentary',
        'label' => 'commentary',
        'rules' => 'min_length[3]',
        'errors' => [
        'required' => 'The commentary field is required',
        ],
      ],
      [
        'field' => 'absenceStatus',
        'label' => 'absenceStatus',
        'rules' => 'in_list[1,2,3]',
        'errors' => [
        'required' => 'The absenceStatus field must contain an 1,2,3',
        ],
      ],
      [
        'field' => 'doctorsNote',
        'label' => 'doctorsNote',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The doctorsNote field must contain an integer',
        ],
      ],     
      [
        'field' => 'substituteId',
        'label' => 'substituteId',
        'rules' => 'integer',
        'errors' => [
        'required' => 'The substituteId field must contain an integer',
        ],
      ],
      [
        'field' => 'startType',
        'label' => 'startType',
        'rules' => 'in_list[0,1]',
        'errors' => [
        'required' => 'The startType field is 0 or 1 required',
        ],
      ],
      [
        'field' => 'endType',
        'label' => 'endType',
        'rules' => 'in_list[0,1]',
        'errors' => [
        'required' => 'The endType field is 0 or 1 required',
        ],
      ],
      [
        'field' => 'status',
        'label' => 'status',
        'rules' => 'in_list[0,1]',
        'errors' => [
        'required' => 'The status field is 0 or 1 required',
        ],
      ]
    ];
    
    $input_data = json_decode($this->input->raw_input_stream, true);
    $this->form_validation->set_data($input_data);
    $this->form_validation->set_rules($config);

    if($this->form_validation->run()==FALSE)
    {
      $message = array(
        'status' => false,
        'message' => validation_errors()
      );
      $this->response($message, REST_Controller::HTTP_NOT_FOUND);
    }else{
      $user_id=$input_data['id'];
      if(isset($input_data['reasonId']))
      {
        $Absences_info['reasonId']=($input_data['reasonId']);
      }
      if(isset($input_data['assignedToId']))
      {
        $Absences_info['assignedToId']=($input_data['assignedToId']);
      }
      if(isset($input_data['approverId']))
      {
        $Absences_info['approverId']=($input_data['approverId']);
      }
      if((isset($input_data['start'])) || (isset($input_data['end'])))
      {
        //qqq

         if($input_data['start'] >= $input_data['end'])
           {
            echo "end date should be greter than and equal to  start date";
            return false;
           }
        $Absences_info['start']=($input_data['start']);
        $Absences_info['end']=($input_data['end']);

      }
      
      if(isset($input_data['commentary']))
      {
        $Absences_info['commentary']=($input_data['commentary']);
      }
      if(isset($input_data['denyReason']))
      {
        $Absences_info['denyReason']=($input_data['denyReason']);
      }
      if(isset($input_data['absenceStatus']))
      {
        $Absences_info['absenceStatus']=($input_data['absenceStatus']);
      }
      if(isset($input_data['doctorsNote']))
      {
        $Absences_info['doctorsNote']=($input_data['doctorsNote']);
      }
      if(isset($input_data['substituteId']))
      {
        $Absences_info['substituteId']=($input_data['substituteId']);
      }
      if(isset($input_data['startType']))
      {
        $Absences_info['startType']=($input_data['startType']);
      }
      if(isset($input_data['endType']))
      {
        $Absences_info['endType']=($input_data['endType']);
      }
      if(isset($input_data['status']))
      {
        $Absences_info['status']=($input_data['status']);
      }
      
      $user_info= $this->session->get_userdata('sessiondata');
        //check session data present or not 
        if(isset($user_info['usersessiondata']))
        {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
          $updatedBy=$user_info['usersessiondata']['id'];
          $Absences_info['updatedBy']=$updatedBy;
          
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          //check id is null or empty
          if($user_id == null || $user_id ==''){
            $message = [
              'status' => true,
              'message' => "Please enter id",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $outputID = $this->Absences_model->get_createdBy($user_id);
            //check id is present or not in db
            if(count($outputID)== null){
              $message = [
                'status' => true,
                'message' => "Id not present",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              // echo "pp";
             // echo "<pre>";  print_r($checkuserauth); exit();

                //check user role 
              if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' ||$checkuserauth == 'HR' ){
                $Absences_info['updateOn']=date('Y-m-d H:i:s');
                $update_reasion= $this->Absences_model->update_Absences($user_id, $Absences_info);
                if($update_reasion){
                $this->response(array(
                  'status'=>1,
                  'message'=>'Absences are updated successfully'
                  ),REST_Controller::HTTP_OK);
                }
                else{
                $this->response(array(
                  'status'=>1,
                  'message'=>'feilds are not updated'
                  ),REST_Controller::HTTP_NOT_FOUND);
                }
              }else{
                $message = [
                  'status' => false,
                  'message' => "your not authorize to add data"
                ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
              }
            }
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK); 
        }
    }
    

  }


}
 ?>
