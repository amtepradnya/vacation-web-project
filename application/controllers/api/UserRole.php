<?php

require APPPATH.'libraries/REST_Controller.php';

class UserRole extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/UserRole_model"));
  }

    public function index_get()
    {
        $user = $this->UserRole_model->get_userRole();
        if($user){
            $message = [
                'status' => true,
                'data' => $user,
                'message' => "All Data",
                ];
            $this->response($message, REST_Controller::HTTP_OK);
        }else{
            $this->response(array(
            'status'=>1,
            'message'=>'fields are not present'
            ),REST_Controller::HTTP_NOT_FOUND);
        }
    } 

}
 ?>
