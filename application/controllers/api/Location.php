<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
class Location extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/Location_model"));
    $this->load->model(array("api/ValidTokenUser_model"));
  }

    public function index_post()
    {
      $headers = $this->input->request_headers();
      $user_info= $this->session->get_userdata('usersessiondata');
      //check user session 
      if(isset($user_info['usersessiondata']))
      {
        $userrole=$user_info['usersessiondata']['role'];
        $usertoken=$user_info['usersessiondata']['token'];
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        //check owner 
        if($checkuserauth == 'Owner'){
        $createdBy=$user_info['usersessiondata']['id'];
        $name = $this->security->xss_clean($this->input->post("name"));
        $status = $this->security->xss_clean($this->input->post("status")); 

        $this->form_validation->set_rules("name", "name", "min_length[3]|required");
        $this->form_validation->set_rules("status", "status", "in_list[0,1]");
        
        if($this->form_validation->run() === FALSE){
          $message = array(
            'status' => false,
            'message' => validation_errors()
          );
          $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else{
            $locationData = array(
                "name" => $name,
                "createdBy" => $createdBy,
                "status" => $status
            );
            //insert data
            if($this->Location_model->insert_location($locationData))
            {
                $this->response(array(
                'status'=>1,
                    'message'=>'location feilds are inserted successfully'
                ),REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                'status'=>0,
                    'message'=>'data not inserted'),
                    REST_Controller::HTTP_NOT_FOUND);
            }
        } 
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }
    }

    public function index_get()
    {
      $headers = $this->input->request_headers();
      $user_info= $this->session->get_userdata('usersessiondata');
      //check session
      if(isset($user_info['usersessiondata']))
     {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
          $checkuserauth= $this->ValidTokenUser_model->checkuser_role($userrole,$usertoken);
          //check user,admin,hr,owner or not
          if($checkuserauth != 'Invalid Token'){
            //get data
            $user = $this->Location_model->get_location();
            if($user){
              $message = [
                  'status' => true,
                  'data' => $user,
                  'message' => "All Data",
                  ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                'status'=>1,
                'message'=>'fields are not present'
                ),REST_Controller::HTTP_NOT_FOUND);
            }
          }else if($checkuserauth == 'Invalid Token'){
              $message = [
                  'status' => true,
                  'message' => "Invalid Token",
                ];
                $this->response($message, REST_Controller::HTTP_OK); 
          }
     }else{
      $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
     }
      
    } 

    public function index_put()
    {
      $user_info=[];
      $user_id='';
      $config = [
        
        [
          'field' => 'status',
          'label' => 'status',
          'rules' => 'in_list[0,1]',
          'errors' => [
          'required' => 'The status field is 0 or 1 required',
          ],
        ],
      ];   
      $input_data = json_decode($this->input->raw_input_stream, true);
      $this->form_validation->set_data($input_data);
      $this->form_validation->set_rules($config);
      if($this->form_validation->run()==FALSE)
      {
        $message = array(
          'status' => false,
          'message' => validation_errors()
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }else{
        $user_id=$input_data['id'];
        if(isset($input_data['name']))
        {
          $user_info['name']=($input_data['name']);
        }
        if(isset($input_data['status']))
        {
          $user_info['status']=($input_data['status']);
        }
        
      }
      $location_info= $this->session->get_userdata('usersessiondata');
      //check session data 
      if(isset($location_info['usersessiondata']))
      {
        $userrole=$location_info['usersessiondata']['role'];
        $usertoken=$location_info['usersessiondata']['token'];
        $usersessionId=$location_info['usersessiondata']['id'];
        
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        //check hr admin owner or user   
        if($checkuserauth == 'Owner'){
          
          $updatedBy=$location_info['usersessiondata']['id'];
          $user_info['updatedBy']=$updatedBy;
          $user_info['updateOn']=date('Y-m-d H:i:s');
          //check id is null or not
          if($user_id == null || $user_id == ''){
            $message = [
              'status' => true,
              'message' => "Id not exist",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $outputID = $this->Location_model->get_createdBy($user_id);
           //check id is present in db 
            if(count($outputID)== null){
              $message = [
                'status' => true,
                'message' => "Id not exist",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              $update_reasion=$this->Location_model->update_location($user_id, $user_info);
              if($update_reasion) {
                $this->response(array(
                  'status'=>1,
                  'message'=>'Location are updated successfully'
                  ),REST_Controller::HTTP_OK);
              } 
            }
          }
         }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }
      
  }
    public function index_delete()
    {
      $data = json_decode(file_get_contents("php://input"));
      $id = $this->security->xss_clean($data->id);
      
      $location_info= $this->session->get_userdata('usersessiondata');
      //check user session is present or not
      if(isset($location_info['usersessiondata']))
       {
        $userrole=$location_info['usersessiondata']['role'];
        $usertoken=$location_info['usersessiondata']['token'];
        $usersessionId=$location_info['usersessiondata']['id'];
        
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        //check id is null or not
        if($id == null || $id ==''){
          $message = [
            'status' => true,
            'message' => "Please enter id",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }else{
          $outputID = $this->Location_model->get_createdBy($id);
          //id is present or not in db
          if(count($outputID)== null){
            $message = [
              'status' => true,
              'message' => "Id not present",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            //check hr,user,admin present 
            if($checkuserauth == 'Owner'){
              if($this->Location_model->delete_location($id)) {
                $this->response(array(
                  'status'=>1,
                  'message'=>'Location are deleted successfully'
                  ),REST_Controller::HTTP_OK);  
              }
              else{
                $this->response(array(
                  'status'=>0,
                  'message'=>'failed to delete feilds'
                  ),REST_Controller::HTTP_NOT_FOUND); 
              }
            }else{
              $message = [
                'status' => true,
                'message' => "unauthorise user",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }
          }
        }
        
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }  
     
    }
}
 ?>
