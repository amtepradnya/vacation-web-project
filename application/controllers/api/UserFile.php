<?php

require APPPATH.'libraries/REST_Controller.php';

class UserFile extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/UserFile_model"));
  }

    public function index_post()
    {
        header("Access-Control-Allow-Origin: *");
        $UserFiles_info= $this->session->get_userdata('sessiondata');
        $createdBy=$UserFiles_info['sessiondata']['user_id'];
        
        $fileId = $this->security->xss_clean($this->input->post("fileId"));
        $status = $this->security->xss_clean($this->input->post("status")); 

        $this->form_validation->set_rules("fileId", "fileId", "required");
        $this->form_validation->set_rules("status", "status", "in_list[0,1]");
        
        if($this->form_validation->run() === FALSE){
          $message = array(
            'status' => false,
            'message' => validation_errors()
          );
          $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else{
            $UserFileDetails = array(
                "fileId" => $fileId,
                "createdBy" => $createdBy,
                "status" => $status
            );

            if($this->UserFile_model->insert_userfile($UserFileDetails))
            {
                $this->response(array(
                'status'=>1,
                    'message'=>'UserFile feilds are added successfully'
                ),REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                'status'=>0,
                    'message'=>'data not added'),
                    REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function index_get()
    {
        $UserFiles = $this->UserFile_model->get_userfile();
        if($UserFiles){
          $message = [
              'status' => true,
              'data' => $UserFiles,
              'message' => "All Data",
              ];
          $this->response($message, REST_Controller::HTTP_OK);
      }else{
          $this->response(array(
          'status'=>1,
          'message'=>'fields are not present'
          ),REST_Controller::HTTP_NOT_FOUND);
      }
    } 

    public function index_put()
    {
      $UserFiles_info=[];
      $config = [
        [
          'field' => 'fileId',
          'label' => 'fileId',
          'rules' => 'required',
          'errors' => [
          'required' => 'The fileId field is required',
          ],
        ],
        [
          'field' => 'status',
          'label' => 'status',
          'rules' => 'in_list[0,1]',
          'errors' => [
          'required' => 'The status field is 0 or 1 required',
          ],
        ],
      ];   
      $input_data = json_decode($this->input->raw_input_stream, true);
      $this->form_validation->set_data($input_data);
      $this->form_validation->set_rules($config);

      if($this->form_validation->run()==FALSE)
      {
        $message = array(
          'status' => false,
          'message' => validation_errors()
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }else{
        $user_id=$input_data['id'];
        if(isset($input_data['fileId']))
        {
          $UserFiles_info['fileId']=($input_data['fileId']);
        }
        if(isset($input_data['status']))
        {
          $UserFiles_info['status']=($input_data['status']);
        }
         $UserFileData_info= $this->session->get_userdata('sessiondata');
          $updatedBy=$UserFileData_info['sessiondata']['user_id'];
          $UserFiles_info['updatedBy']=$updatedBy;
          $UserFiles_info['updateOn']=date('Y-m-d H:i:s');
          $update_reasion=$this->UserFile_model->update_userfile($user_id, $UserFiles_info);
          if($update_reasion){
          $this->response(array(
            'status'=>1,
             'message'=>'UserFile are updated successfully'
            ),REST_Controller::HTTP_OK);
          }else{
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are not updated'
            ),REST_Controller::HTTP_NOT_FOUND);
          }
      }
       
    }
    public function index_delete()
    {
        $data = json_decode(file_get_contents("php://input"));

      $id = $this->security->xss_clean($data->id);
         // print_r($id);exit();
      
        if($this->UserFile_model->delete_UserFile($id)) {
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are deleted successfully'
            ),REST_Controller::HTTP_OK);  
        }
        else{
          $this->response(array(
            'status'=>0,
             'message'=>'failed to delete feilds'
            ),REST_Controller::HTTP_NOT_FOUND); 
        }
    }
}
 ?>
