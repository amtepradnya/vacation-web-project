<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
class Holiday extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/Holiday_model"));
    $this->load->model(array("api/ValidTokenUser_model"));
  }
    public function index_post()
    {
      $headers = $this->input->request_headers();
      $user_info= $this->session->get_userdata('usersessiondata');
        //check user session 
      if(isset($user_info['usersessiondata']))
      {
        $userrole=$user_info['usersessiondata']['role'];
        $usertoken=$user_info['usersessiondata']['token'];
        $createdBy=$user_info['usersessiondata']['id'];
        
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          //check valid user,hr,admin and owner 
        if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' || $checkuserauth == 'HR'){
          $name = $this->security->xss_clean($this->input->post("name"));
          $holidaydate = $this->security->xss_clean($this->input->post("holidaydate"));
          $status = $this->security->xss_clean($this->input->post("status")); 

          $this->form_validation->set_rules("name", "name", "required");
          $this->form_validation->set_rules("holidaydate", "holidaydate", "max_length[10]|is_start_date_valid|trim|required");
          $this->form_validation->set_rules("status", "status", "in_list[0,1]");
          
          if($this->form_validation->run() === FALSE){
            $message = array(
              'status' => false,
              'message' => validation_errors()
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
          }else{
              $HolidayDetails = array(
                  "name" => $name,
                  "holidaydate"=>$holidaydate,
                  "createdBy" => $createdBy,
                  "status" => $status
              );

              if($this->Holiday_model->insert_Holiday($HolidayDetails))
              {
                $message = [
                  'status' => true,
                  'message' => "Holiday feilds are added successfully",
                ];
                $this->response($message, REST_Controller::HTTP_OK);  
              }else{
                $message = [
                  'status' => true,
                  'message' => "feilds not added",
                ];
                $this->response($message, REST_Controller::HTTP_OK); 
              }
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }
    }

    public function index_get()
    {
      $headers = $this->input->request_headers();
      $user_info= $this->session->get_userdata('usersessiondata');
      if(isset($user_info['usersessiondata']))
     {
      $userrole=$user_info['usersessiondata']['role'];
      $usertoken=$user_info['usersessiondata']['token'];
     
      $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
      if($checkuserauth != 'Invalid Token' || $checkuserauth != 'Invalid User'){
        $Holidays = $this->Holiday_model->get_holiday();
        if($Holidays){
          $message = [
              'status' => true,
              'data' => $Holidays,
              'message' => "All Data",
              ];
          $this->response($message, REST_Controller::HTTP_OK);
        }else{
          $message = [
            'status' => true,
            'message' => "fields are not present",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }      
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }
     }else{
      $message = [
        'status' => true,
        'message' => "unauthorise user",
      ];
      $this->response($message, REST_Controller::HTTP_OK);
     }
        
    } 

    public function index_put()
    {
      $Holidays_info=[];
      $user_id='';
      $user_info= $this->session->get_userdata('usersessiondata');
      $config = [
        [
          'field' => 'name',
          'label' => 'name',
          'errors' => [
          'required' => 'The name field is required',
          ],
        ],
        [
            'field' => 'holidaydate',
            'label' => 'holidaydate',
            'rules' => 'max_length[10]|is_start_date_valid|trim',
            'errors' => [
            'required' => 'The holidaydate field is required',
            ],
          ],
        [
          'field' => 'status',
          'label' => 'status',
          'rules' => 'in_list[0,1]',
          'errors' => [
          'required' => 'The status field is 0 or 1 required',
          ],
        ],
      ];   
      $input_data = json_decode($this->input->raw_input_stream, true);
      $this->form_validation->set_data($input_data);
      $this->form_validation->set_rules($config);
      if($this->form_validation->run()==FALSE)
      {
        $message = array(
          'status' => false,
          'message' => validation_errors()
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }else{
        $user_id=$input_data['id'];
        if(isset($input_data['name']))
        {
          $Holidays_info['name']=($input_data['name']);
        }
        if(isset($input_data['holidaydate']))
        {
          $Holidays_info['holidaydate']=($input_data['holidaydate']);
        }
        if(isset($input_data['status']))
        {
          $Holidays_info['status']=($input_data['status']);
        }
      }
      //check session data present or not 
     if(isset($user_info['usersessiondata']))
      {
        $userrole=$user_info['usersessiondata']['role'];
        $usertoken=$user_info['usersessiondata']['token'];
        $usersessionId=$user_info['usersessiondata']['id'];
        
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        //check user role 
        if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' || $checkuserauth == 'HR'){
          $Holidays_info['updatedBy']=$usersessionId;
          $Holidays_info['updateOn']=date('Y-m-d H:i:s');
          if($user_id == null || $user_id ==''){
            $message = [
              'status' => true,
              'message' => "Id not exist",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $outputID = $this->Holiday_model->get_createdBy($user_id);
            if(count($outputID)== null){
              $message = [
                'status' => true,
                'message' => "Id not exist",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              $update_reasion=$this->Holiday_model->update_Holiday($user_id, $Holidays_info);
              if($update_reasion){
                $message = [
                  'status' => true,
                  'message' => "Holiday are updated successfully",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
              }else{
                $message = [
                  'status' => true,
                  'message' => "feilds are not updated",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
              }
            }
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }      
       
    }
    public function index_delete()
    {
      $data = json_decode(file_get_contents("php://input"));
      $id = $this->security->xss_clean($data->id);
      
      $user_info= $this->session->get_userdata('usersessiondata');
      //check session
      if(isset($user_info['usersessiondata']))
      {
        $userrole=$user_info['usersessiondata']['role'];
        $usertoken=$user_info['usersessiondata']['token'];
        $usersessionId=$user_info['usersessiondata']['id'];
        
       $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
       //check user role
        if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' || $checkuserauth == 'HR'){
          if($id == null || $id ==''){
            $message = [
              'status' => true,
              'message' => "Id not exist",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $outputID = $this->Holiday_model->get_createdBy($id);
            if(count($outputID)== null){
              $message = [
                'status' => true,
                'message' => "Id not exist",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              if($this->Holiday_model->delete_Holiday($id)) {
                $this->response(array(
                  'status'=>1,
                   'message'=>'Holiday are deleted successfully'
                  ),REST_Controller::HTTP_OK);  
              }
              else{
                $this->response(array(
                  'status'=>0,
                   'message'=>'failed to delete feilds'
                  ),REST_Controller::HTTP_NOT_FOUND); 
              }
            }
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }
    }
}
 ?>
