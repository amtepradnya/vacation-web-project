<?php

require APPPATH.'libraries/REST_Controller.php';

class DepartmentEmails extends REST_Controller{

  public function __construct(){

    parent::__construct();
    $this->load->database();
    $this->load->model(array("api/DepartmentEmails_model"));
    $this->load->model(array("api/user_model"));

  }

    public function index_post()
    {
      header("Access-Control-Allow-Origin: *");

      
   $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {

        $createdBy=$user_info['usersessiondata']['id'];
        
        $deptId = $this->security->xss_clean($this->input->post("deptId"));
        $userId = $this->security->xss_clean($this->input->post("userId"));
        $email = $this->security->xss_clean($this->input->post("email"));
        $status=$this->security->xss_clean($this->input->post("status")); 

        $this->form_validation->set_rules("deptId", "deptId", "integer|required");
        $this->form_validation->set_rules("userId", "userId", "integer");
        $this->form_validation->set_rules("email", "email", "valid_email|required");
        $this->form_validation->set_rules("status", "status", "in_list[0,1]");
        
        if($this->form_validation->run() === FALSE){
          $message = array(
            'status' => false,
            'message' => validation_errors()
          );
          $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else{
            $departmentemail = array(
                "deptId" => $deptId,
                "userId" => $userId,
                "email" => $email,
                "createdBy" => $createdBy,
                "status" => $status
            );
// print_r($departmentemail); exit();
            if($this->DepartmentEmails_model->insert_DepartmentEmails($departmentemail))
            {
                $this->response(array(
                'updatedBy'=>1,
                    'message'=>'data added successfully'
                ),REST_Controller::HTTP_OK);
            }else{
                $this->response(array(
                'updatedBy'=>0,
                    'message'=>'data not added'),
                    REST_Controller::HTTP_NOT_FOUND);
            }
        }
      }else{

          $message = [
           'status' => false,
                    'message' => "your not authorize to add data"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
      }
    }
    else{
        $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }else{

      $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
  }
}

    public function index_get()
    {
         $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {

        $departmentEmails = $this->DepartmentEmails_model->get_DepartmentEmails();
        if($departmentEmails){
          $message = [
              'status' => true,
              'data' => $departmentEmails,
              'message' => "All Data",
              ];
          $this->response($message, REST_Controller::HTTP_OK);
      }else{
          $this->response(array(
          'status'=>1,
          'message'=>'fields are not present'
          ),REST_Controller::HTTP_NOT_FOUND);
      }
    }else{
$message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

    }
    } else{

      $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }else{

      $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
  }
}
  
    public function index_delete()
    {
               $user_info= $this->session->get_userdata('usersessiondata');


       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {

          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {

      $data = json_decode(file_get_contents("php://input"));
      $deptId = $this->security->xss_clean($data->deptId);
      $userId = $this->security->xss_clean($data->userId);
       $idexist= $this->DepartmentEmails_model->checkid_exist($deptId,$userId);
    if(!empty($idexist))
            {
      $output=$this->DepartmentEmails_model->delete_DepartmentEmails($deptId,$userId); 
      if($output)
      {
        $this->response(array(
         'status'=>1,
          'message'=>'feilds are deleted successfully'
         ),REST_Controller::HTTP_OK);  
      }else{
       $this->response(array(
         'status'=>0,
          'message'=>'failed to delete feilds'
         ),REST_Controller::HTTP_NOT_FOUND); 
      }
    }else{
     $message = [
           'status' => false,
                    'message' => "Id not exist"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

    }
    }else
    {

      $message = [
           'status' => false,
                    'message' => "your not authorize user to delete data"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }else{


      $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
  }
}else{

    $message = [
           'status' => false,
                    'message' => "unauthorize user"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
} 
}
}
 ?>
