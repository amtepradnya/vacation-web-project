<?php

require APPPATH.'libraries/REST_Controller.php';

class Reg_employee extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->helper('security');

    // $this->load->library('security');
    $this->load->database();
    $this->load->model(array("api/employee_model"));
  }

  public function index_post()
  {
    // $data = $this->security->xss_clean($_POST);
  
    $fullname = $this->security->xss_clean($this->input->post("fullname"));
    $email = $this->security->xss_clean($this->input->post("email"));
    $phone = $this->security->xss_clean($this->input->post("phone"));
    $address = $this->security->xss_clean($this->input->post("address")); 
    $profile = $this->security->xss_clean($this->input->post("profile")); 
    $gender = $this->security->xss_clean($this->input->post("gender")); 
    $password = $this->security->xss_clean($this->input->post("password")); 
    $newpassword=md5($password);
    
    // echo $fullname;
     $this->form_validation->set_rules("fullname", "fullname", "required");
     $this->form_validation->set_rules("email", "email", "required");
     $this->form_validation->set_rules("phone", "phone", "required");
     $this->form_validation->set_rules("address", "address", "required");
     $this->form_validation->set_rules("profile", "profile", "required");
     $this->form_validation->set_rules("gender", "gender", "required");
     $this->form_validation->set_rules("password", "password", "required");

     

     if($this->form_validation->run() === FALSE){
      $this->response(array(
        'status'=>0,
         'message'=>'all feilds are needed'),
         REST_Controller::HTTP_NOT_FOUND);
     }else{
        $employees = array(
          "fullname" => $fullname,
          "email" => $email,
          "phone" => $phone,
          "address" => $address,
          "profile" => $profile,
          "gender" => $gender,
          "password"=>$newpassword
        );

     if($this->employee_model->insert_data($employees))
          {
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are inserted successfully'
            ),REST_Controller::HTTP_OK);

          }
      }
      
}

public function index_get()
{
  $employee = $this->employee_model->get_employee();
  print_r($employee);

} 

public function index_put()
{
 $data = json_decode(file_get_contents("php://input"));
 $employee_info=[];
  // echo "<pre>"; print_r($employee_info);exit();
 if(isset($data->id))
 {
  
  $employee_id=$data->id;
  if(isset($data->fullname))
  {
    $employee_info['fullname']=($data->fullname);
   }
  if(isset($data->email))
  {
    $employee_info['email']=($data->email);
  }
  if(isset($data->phone))
  {
    $employee_info['phone']=($data->phone);
  }
  if(isset($data->address))
  {
    $employee_info['address']=($data->address);
  }
  if(isset($data->profile))
  {
    $employee_info['profile']=($data->profile);
  }
  if(isset($data->gender))
  {
    $employee_info['gender']=($data->gender);
  }
    //  echo "<pre>"; print_r($employee_info);exit();



  if($this->employee_model->update_data($employee_id, $employee_info)) {
          $this->response(array(
            'status'=>1,
             'message'=>'feilds are updated successfully'
            ),REST_Controller::HTTP_OK);

          }
 }
 else
 {
  $this->response(array(
    'status'=>0,
     'message'=>'feilds are needed'
    ),REST_Controller::HTTP_NOT_FOUND); 
  }

}
public function index_delete()
{
  // $data = json_decode(file_get_contents("php://input"));
  // print_r($data); exit();
  // $emp_id = $this->security->xss_clean($data->id);
  // if($this->employee_model->delete_data($emp_id)) {
  //   $this->response(array(
  //     'status'=>1,
  //      'message'=>'feilds are deleted successfully'
  //     ),REST_Controller::HTTP_OK);  
  // }
  // else{
  //   $this->response(array(
  //     'status'=>0,
  //      'message'=>'failed to delete feilds'
  //     ),REST_Controller::HTTP_NOT_FOUND); 
  // }


}
}
 ?>
