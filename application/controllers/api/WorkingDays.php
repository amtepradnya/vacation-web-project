<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class WorkingDays extends REST_Controller {

	 public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/WorkingDays_model"));
        $this->load->model(array("api/user_model"));

            
    }
    public function workingsdays_get()
    {
        $user_info= $this->session->get_userdata('usersessiondata');
        if(!empty($user_info['usersessiondata']))
       {
    $workingDays=
     $this->WorkingDays_model->get_workingdays();
      if($workingDays)
            {
              $message = [
                  'status' => true,
                  'data' => $workingDays,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
          
     }else{


           $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);   

     }
   }
    public function userworkingdays_get()
    {
      header("Access-Control-Allow-Origin: *");
        
       $user_info= $this->session->get_userdata('usersessiondata');
        if(!empty($user_info['usersessiondata']))
       {
       $usertoken=$user_info['usersessiondata']['token'];
       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       $userid=$user_info['usersessiondata']['id'];
       $userrole=$user_info['usersessiondata']['role'];
       if(empty(!$checkuserauth))
       {
        $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          

    $workingDays=
     $this->WorkingDays_model->get_userworkingdays();
      if($workingDays)
            {
              $message = [
                  'status' => true,
                  'data' => $workingDays,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
          
     }else{

  $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
     }
   }else{


  $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
   }
 }



public function deleteusrworkdays_delete()
{

  header("Access-Control-Allow-Origin: *");
        
       $user_info= $this->session->get_userdata('usersessiondata');
        if(!empty($user_info['usersessiondata']))
       {
       $usertoken=$user_info['usersessiondata']['token'];
       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       $userid=$user_info['usersessiondata']['id'];
       $userrole=$user_info['usersessiondata']['role'];
       if(empty(!$checkuserauth))
       {
        $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {
     $data = json_decode(file_get_contents("php://input"));

  $workingId = $this->security->xss_clean($data->workingId);
  $user_id= $this->security->xss_clean($data->userId);

  $idexist= $this->WorkingDays_model->checkid_exist($workingId, $user_id);
         // echo "<pre>";  print_r($idexist); 
           if(!empty($idexist))
          {

   // print_r($user_id);exit();

  $output=$this->WorkingDays_model->delete_usrWorkingDays($workingId, $user_id);
  if($output) {
    $this->response(array(
      'status'=>1,
       'message'=>'feilds are deleted successfully'
      ),REST_Controller::HTTP_OK);  
  }
  else{
    $this->response(array(
      'status'=>0,
       'message'=>'failed to delete feilds'
      ),REST_Controller::HTTP_NOT_FOUND); 
  }
}else{

   $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);

}

  }else{

   $message = [
                  'status' => false,
                    'message' => "your not authorize to delete data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
  }
}else
{
     $message = [

          'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

}
}else{
     $message = [

   'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}
}
    public function saveuserWorkingDays_post()
    {

       $WorkingDays_arr= [];
       // $user_arr= array();

 header("Access-Control-Allow-Origin: *");
        
       $user_info= $this->session->get_userdata('usersessiondata');
        if(!empty($user_info['usersessiondata']))
       {
       $usertoken=$user_info['usersessiondata']['token'];
       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       $userid=$user_info['usersessiondata']['id'];
       $userrole=$user_info['usersessiondata']['role'];
       if(empty(!$checkuserauth))
       {
        $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {
        // $_POST = $this->security->xss_clean($_POST);
          $workingId = $this->security->xss_clean($this->input->post("workingId")); 
          $userId = $this->security->xss_clean($this->input->post("userId")); 

          $this->form_validation->set_rules("workingId", "workingId", "integer|required");
          $this->form_validation->set_rules("userId", "userId", "integer|required");
          $this->form_validation->set_rules("status", "status", "integer|in_list[0,1]");

            if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else
      {
         $status=$this->security->xss_clean($this->input->post("status")); 
         $WorkingDays_arr['workingId']=  $workingId;
         $WorkingDays_arr['userId']=  $userId;
         $WorkingDays_arr['status']=  $status;

           
          
           $updatedbyid=$userid;
           $WorkingDays_arr['createdBy']=$updatedbyid;
           
          
     $output= $this->WorkingDays_model->save_userWorkingDays($WorkingDays_arr);
            if($output)
            {
              $message = [
                  'status' => true,
                  'data' => $WorkingDays_arr,
                    'message' => "Data Inserted successfully",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }else
            {
              $message = [
                  'status' => false,
                    'message' => "failed to add data",
                         ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);

            }

           }
         }else{

            $message = [
                  'status' => false,
                    'message' => "your not authorize to add data"
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
         }
       }else{

          $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
       }
     }else{

        $message = [
                  'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
     }

}
}
?>