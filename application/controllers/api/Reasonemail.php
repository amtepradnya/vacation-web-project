<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'libraries/REST_Controller.php';


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Reasonemail extends REST_Controller {
	 public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/Reasonemail_model"));
        $this->load->model(array("api/user_model"));

        
            
    }
        public function index_get()
    {

       $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
    $reason =
     $this->Reasonemail_model->get_Reasonemail();
      if($reason)
            {
              $message = [
                  'status' => true,
                  'data' => $reason,
                    'message' => "All Data",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
         
       }else 
       {

         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
       }
       }else{

         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
       } 
     }else{
           
           $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

     }
     }
      
     public function index_post()
    {
      $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
       $reason_arr= array();

    header("Access-Control-Allow-Origin: *");
        // $_POST = $this->security->xss_clean($_POST);
        $reaseonid = $this->security->xss_clean($this->input->post("reaseonid")); 
        $email = $this->security->xss_clean($this->input->post("email"));         
        $userId = $this->security->xss_clean($this->input->post("userId"));         
       
        $this->form_validation->set_rules("reaseonid", "reaseonid", "integer|required");
        $this->form_validation->set_rules("userId", "userId", "integer");
        $this->form_validation->set_rules("email", "email", "valid_email|required");
        $this->form_validation->set_rules("status", "status", "in_list[0,1]");

        if ($this->form_validation->run() == FALSE)
        {  // Form Validation Errors
            $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else
      {
          $reason_arr['reaseonid']=  $reaseonid;
          $status = $this->security->xss_clean($this->input->post("status")); 
            $reason_arr['status']=  $status;
            $reason_arr['email']=  $email;

           

           $user_info= $this->session->get_userdata('sessiondata');
           // $useremail=$user_info['sessiondata']['email'];

           $reason_arr['createdBy']=$sessionuserid;
           $reason_arr['userId']=$userId;
          

             // echo "<pre>"; print_r($reason_arr);exit();

         $output= $this->Reasonemail_model->insert_reasonemail($reason_arr);
            if($output)
            {
              $message = [
                  'status' => true,
                  'data' => $reason_arr,
                    'message' => "Data added successfully",
                         ];
                $this->response($message, REST_Controller::HTTP_OK);
            }

           }
         }else{

           $message = [
                    'status' => false,
                    'message' => "your not authorise to add data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
         }
       }else
       {
         $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
       }
     }else{
      $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 

     }
}
    public function index_delete()
    {
      $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
       // print_r($user_info['sessiondata']);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuserid=$user_info['usersessiondata']['id'];

        // print_r($user); exit();

       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
           
        $checkrole= $this->user_model->checkuser_role($userrole);

       if($checkrole[0]->role != "User")
       {
       $reason_arr= array();
    $config = [
            [
            'field' => 'reaseonid',
            'label' => 'reaseonid',
            'rules' => 'required',
            'errors' => [
                    'required' => 'The reaseonid field is required',
            ], 
          ],
          [
            'field' => 'userId',
            'label' => 'userId',
            'rules' => 'required',
            'errors' => [
                    'required' => 'The userId field is required',
            ], 
          ],
          [
            'field' => 'email',
            'label' => 'email',
            'rules' => 'valid_email',
            'errors' => [
                    'required' => 'The userId field is required',
            ], 
          ],
                        
];
 $input_data = json_decode($this->input->raw_input_stream, true);

 $this->form_validation->set_data($input_data);
 $this->form_validation->set_rules($config);
 if($this->form_validation->run()==FALSE)
 {
 $message = array(
                'status' => false,
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);

}else{
          $reason_id=$input_data['reaseonid'];
          $userid=$input_data['userId'];
             $idexist= $this->Reasonemail_model->checkid_exist($reason_id,$userid);
    if(!empty($idexist))
            {

            $delete_reasion= 
    $this->Reasonemail_model->delete_reasonemail($reason_id, $userid);
    // print_r($delete_reasion);exit();
    if($delete_reasion){
          $this->response(array(
            'status'=>1,
             'message'=>'feilds deleted successfully'
            ),REST_Controller::HTTP_OK);

          }
          else{
          $this->response(array(
            'status'=>1,
             'message'=>'feilds to delete'
            ),REST_Controller::HTTP_NOT_FOUND);
          }
         }
         else{
                       $message = [
                    'status' => false,
                    'message' => "Id not exist",
                         ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 

         }
          
        }  
    }else{
        $message = [
                    'status' => false,
                    'message' => "your not authorise to delete data",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
    }
  }else{

      $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
  }
}else{

    $message = [
                    'status' => false,
                    'message' => "unauthorise user",
                         ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED); 
}
}
}
?>