<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Allowances extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/Allowances_model"));
    $this->load->model(array("api/ValidTokenUser_model"));
  }

    public function index_post()
    {
      $headers = $this->input->request_headers();
      $allowances_info= $this->session->get_userdata('usersessiondata');
        //check session 
        if(isset($allowances_info['usersessiondata']))
        {
          $userrole=$allowances_info['usersessiondata']['role'];
          $usertoken=$allowances_info['usersessiondata']['token'];
          $createdBy=$allowances_info['usersessiondata']['id'];
      
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          //check owner 
          if($checkuserauth == 'Owner' || $checkuserauth == 'Admin' || $checkuserauth == 'HR'){
            $initialAllowance = $this->security->xss_clean($this->input->post("initialAllowance"));
            $name = $this->security->xss_clean($this->input->post("name"));
            $residualLeaveAvailable = $this->security->xss_clean($this->input->post("residualLeaveAvailable"));
            $status = $this->security->xss_clean($this->input->post("status")); 

            $this->form_validation->set_rules("initialAllowance", "initialAllowance", "integer|required");
            $this->form_validation->set_rules("name", "name", "required");
            $this->form_validation->set_rules("residualLeaveAvailable", "residualLeaveAvailable", "in_list[0,1]|required");
            $this->form_validation->set_rules("status", "status", "in_list[0,1]");
            
            
            if($this->form_validation->run() === FALSE){
              $message = array(
                'status' => false,
                'message' => validation_errors()
              );
              $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }else{
                $allowancesData = array(
                  "initialAllowance" => $initialAllowance,
                  "name" => $name,
                  "residualLeaveAvailable" => $residualLeaveAvailable,
                  "createdBy" => $createdBy,
                  "status" => $status
                );
                //insert data 
                if($this->Allowances_model->insert_allowances($allowancesData))
                {
                    $this->response(array(
                    'status'=>1,
                        'message'=>'allowances feilds are added successfully'
                    ),REST_Controller::HTTP_OK);
                }else{
                    $this->response(array(
                    'status'=>0,
                        'message'=>'data not added'),
                        REST_Controller::HTTP_NOT_FOUND);
                }
            }
          }else{
            $message = [
              'status' => true,
              'message' => "unauthorise user",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
        
    }

    public function index_get()
    {
        $allowancesData = $this->Allowances_model->get_allowances();

        $headers = $this->input->request_headers();
        $allowances_info= $this->session->get_userdata('usersessiondata');
     
         if(isset($allowances_info['usersessiondata']))
          {

        $userrole=$allowances_info['usersessiondata']['role'];
        $usertoken=$allowances_info['usersessiondata']['token'];
        $checkuserauth= $this->ValidTokenUser_model->checkuser_role($userrole,$usertoken);
            // echo "<pre>"; print_r($usertoken);exit();

            if($checkuserauth != 'Invalid User' || $checkuserauth != 'Invalid Token')
            {
              if($allowancesData){
                $message = [
                  'status' => true,
                  'data' => $allowancesData,
                  'message' => "All Data",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
              }else{
                $this->response(array(
                'status'=>1,
                'message'=>'fields are not present'
                ),REST_Controller::HTTP_NOT_FOUND);
              }
            }else{
              $message = [
                'status' => true,
                'message' => "unauthorise user",
              ];
              $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
            }
        }else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);


    } 
  }

    public function index_put()
    { 
      $allowances_info=[];
      $allowances_id='';
      $user_info= $this->session->get_userdata('usersessiondata');
      $config = [
        [
          'field' => 'initialAllowance',
          'label' => 'initialAllowance',
          'rules' => 'integer'

        ],
        
        [
          'field' => 'residualLeaveAvailable',
          'label' => 'residualLeaveAvailable',
          'rules' => 'integer|in_list[0,1]'
         ],
        [
          'field' => 'status',
          'label' => 'status',
          'rules' => 'integer|in_list[0,1]'
         ],
       ];
      $input_data = json_decode($this->input->raw_input_stream, true);
      $this->form_validation->set_data($input_data);
      $this->form_validation->set_rules($config);
      if($this->form_validation->run()==FALSE)
      {
        $message = array(
          'status' => false,
          'message' => validation_errors()
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
      }else{
        $allowances_id=$input_data['id'];
        if(isset($input_data['initialAllowance']))
        {
<<<<<<< HEAD
          $message = array(
            'status' => false,
            'message' => validation_errors()
          );
          $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }else{
          $allowances_id=$input_data['id'];
             $idexist= $this->Allowances_model->checkid_exist($allowances_id);
            if(!empty($idexist))
          {
                               // echo "<pre>"; print_r($allowances_info);exit();

          if(isset($input_data['initialAllowance']))
          {
            $allowances_info['initialAllowance']=($input_data['initialAllowance']);
          }
          if(isset($input_data['name']))
          {
            $allowances_info['name']=($input_data['name']);
          }
          if(isset($input_data['residualLeaveAvailable']))
          {
            $allowances_info['residualLeaveAvailable']=($input_data['residualLeaveAvailable']);
          }
                    
          if(isset($input_data['status']))
          {
            $allowances_info['status']=($input_data['status']);
          }
       
                     // echo "<pre>"; print_r($allowances_info);exit();

      //check session data present or not 
          $allowances_info['updatedBy']=$usersessionId;
          $allowances_info['updateOn']=date('Y-m-d H:i:s');
=======
          $allowances_info['initialAllowance']=($input_data['initialAllowance']);
        }
        if(isset($input_data['name']))
        {
          $allowances_info['name']=($input_data['name']);
        }
        if(isset($input_data['residualLeaveAvailable']))
        {
          $allowances_info['residualLeaveAvailable']=($input_data['residualLeaveAvailable']);
        }
                  
        if(isset($input_data['status']))
        {
          $allowances_info['status']=($input_data['status']);
        }
      }
>>>>>>> 25b48ff1bc1a7e75ae60b50649bcffbea5cc17eb

      //check session
      if(isset($user_info['usersessiondata']))
      {
        $userrole=$user_info['usersessiondata']['role'];
        $usersessionId=$user_info['usersessiondata']['id'];
        $usertoken=$user_info['usersessiondata']['token'];
        $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
        if($checkuserauth == 'Admin' || $checkuserauth == 'HR' || $checkuserauth == 'Owner')
        {
          //check id is null or not
          if($allowances_id == null || $allowances_id ==''){
            $message = [
              'status' => true,
              'message' => "Id not exist",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }else{
            $outputID = $this->Allowances_model->get_createdBy($allowances_id);
              //check id is present or not in db
              if(count($outputID)== null){
                $message = [
                  'status' => true,
                  'message' => "Id not exist",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
              }else{
                //check session data present or not 
                $allowances_info['updatedBy']=$usersessionId;
                $allowances_info['updateOn']=date('Y-m-d H:i:s');

                if($this->Allowances_model->update_allowances($allowances_id, $allowances_info)) {
                  $this->response(array(
                    'status'=>1,
                    'message'=>'Allowances are updated successfully'
                    ),REST_Controller::HTTP_OK);
                }else{
                      $this->response(array(
                    'status'=>1,
                    'message'=>'Allowances failed to update '
                    ),REST_Controller::HTTP_OK);

                }
              }
          }
<<<<<<< HEAD
       
      }else{
              $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND); 


      }
       }
=======

>>>>>>> 25b48ff1bc1a7e75ae60b50649bcffbea5cc17eb
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }
      }
      else{
        $message = [
          'status' => true,
          'message' => "unauthorise user",
        ];
        $this->response($message, REST_Controller::HTTP_OK);
      }
     
    }
    public function index_delete()
    {
      $data = json_decode(file_get_contents("php://input"));
      $id = $this->security->xss_clean($data->id);
      
      $user_info= $this->session->get_userdata('usersessiondata');
        if(isset($user_info['usersessiondata']))
        {
          $userrole=$user_info['usersessiondata']['role'];
          $usertoken=$user_info['usersessiondata']['token'];
          $checkuserauth= $this->ValidTokenUser_model->checking_role($userrole,$usertoken);
          if($checkuserauth == 'Admin' || $checkuserauth == 'HR' || $checkuserauth == 'Owner'){
<<<<<<< HEAD

            $idexist= $this->Allowances_model->checkid_exist($id);
            if(!empty($idexist))
          {
            if($this->Allowances_model->delete_allowances($id)) {
              $message = [
                'status' => true,
                'message' => "Allowances are deleted successfully",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }
            else{
=======
             //check id is null or not
            if($id == null || $id ==''){
>>>>>>> 25b48ff1bc1a7e75ae60b50649bcffbea5cc17eb
              $message = [
                'status' => true,
                'message' => "Id not exist",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              $outputID = $this->Allowances_model->get_createdBy($id);
              //check id is present or not in db
              if(count($outputID)== null){
                $message = [
                  'status' => true,
                  'message' => "Id not exist",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
              }else{
                if($this->Allowances_model->delete_allowances($id)) {
                  $message = [
                    'status' => true,
                    'message' => "Allowances are deleted successfully",
                  ];
                  $this->response($message, REST_Controller::HTTP_OK);
                }
                else{
                  $message = [
                    'status' => true,
                    'message' => "failed to delete feilds",
                  ];
                  $this->response($message, REST_Controller::HTTP_OK);
                }
              }
            }
          }else{
            $message = [
                    'status' => FALSE,
                    'message' => "Id not exist",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);


          }
          }else{
            $message = [
              'status' => true,
              'message' => "unauthorise user",
            ];
            $this->response($message, REST_Controller::HTTP_OK);
          }
        }else{
          $message = [
            'status' => true,
            'message' => "unauthorise user",
          ];
          $this->response($message, REST_Controller::HTTP_OK);
        }

    }
}
 ?>
