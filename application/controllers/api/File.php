<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
class File extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
        $this->load->library('Authorization_Token');
        $this->load->database();
        $this->load->model(array("api/user_model"));
        $this->load->helper('date');
        $this->load->model(array("api/File_model"));

        
  }
  public function index_delete()
  {
    $user_info= $this->session->get_userdata('usersessiondata');
    if(isset($user_info['usersessiondata']))
    {
       // print_r($user_info['sessiondata']);exit();
      $userrole=$user_info['usersessiondata']['role'];
      $usertoken=$user_info['usersessiondata']['token'];
      $sessionid=$user_info['usersessiondata']['id'];

      $checkuserauth= $this->user_model->checkuser_token($usertoken);
      if(empty(!$checkuserauth))
      {
        $checkrole= $this->user_model->checkuser_role($userrole);
         $data = json_decode(file_get_contents("php://input"));
          $id = $this->security->xss_clean($data->id);
         // print_r($data->id);exit();
        if($checkrole[0]->role == "User" )
        {
            $user_file= $this->File_model->check_userfile($id);
                   // print_r($upra);exit();

          if((isset($user_file[0])) && ($user_file[0]->createdBy == $sessionid))
         {
            
          if($this->File_model->delete_file($id)) {
            $this->response(array(
            'status'=>1,
            'message'=>'feilds are deleted successfully'
            ),REST_Controller::HTTP_OK);  
          }
          else{
            $this->response(array(
            'status'=>0,
            'message'=>'failed to delete feilds'
            ),REST_Controller::HTTP_NOT_FOUND); 
          }

      }else{
        $message = [
            'status' => false,
            'message' => "your not authorize to delete data"
          ];
          $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

      }
      }else{
       
         
             $idexist= $this->File_model->checkid_exist($id);
         if(!empty($idexist)){
          if($this->File_model->delete_file($id)) {
            $this->response(array(
            'status'=>1,
            'message'=>'feilds are deleted successfully'
            ),REST_Controller::HTTP_OK);  
          }
          else{
            $this->response(array(
            'status'=>0,
            'message'=>'failed to delete feilds'
            ),REST_Controller::HTTP_NOT_FOUND); 
          }
        }else{
          $message = [
            'status' => false,
            'message' => "Id not exist"
          ];
          $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
        }
      }
    }else{
      $message = [
        'status' => false,
        'message' => "unauthorize user"
      ];
      $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }else{

  	 $message = [
        'status' => false,
        'message' => "unauthorize user"
      ];
      $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
  }
}

  public function addfile_post()
  {
    $user_info= $this->session->get_userdata('usersessiondata');
    if(!empty($user_info['usersessiondata']))
    {
      $usertoken=$user_info['usersessiondata']['token'];
      $checkuserauth= $this->user_model->checkuser_token($usertoken);
      $userid=$user_info['usersessiondata']['id'];
      $userrole=$user_info['usersessiondata']['role'];
      if(empty(!$checkuserauth))
      {
        
          $config['upload_path']          = './files/';
          $config['allowed_types']        = 'gif|jpg|png';
          $config['max_size']             = 100;
          $config['max_width']            = 1024;
          $config['max_height']           = 768;

          $this->load->library('upload', $config);
         if ( ! $this->upload->do_upload('file'))
          {
            $error = array('error' => $this->upload->display_errors());
            $returndata = array('status'=>404,'data'=>$error,'message'=>'file upload failed');
          }
          else
          {
            $filePath='';
            $data = array('upload_data' => $this->upload->data());
            $filePath=$data['upload_data']['full_path'];
            $file_type=$data['upload_data']['file_type'];
            $uploadData = array("filepath" => $filePath,"filetype" => $file_type, "createdBy"=>$userid);
            $checkuserauth= $this->File_model->add_file($uploadData);
            if($checkuserauth){
              $message = [
                'status' => true,
                'data' => $uploadData,
                'message' => "image Added successfully",
              ];
              $this->response($message, REST_Controller::HTTP_OK);
            }else{
              $message = [
                'status' => false,
                'message' => "failed to Add file",
              ];
              $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
          }
       
      }else{
        $message = [
          'status' => FALSE,
          'message' => " unauthorize user",
        ];
        $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
      }  
    }else{
      $message = [
        'status' => FALSE,
        'message' => " unauthorize user",
      ];
      $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
    }
  }

  public function imageupload_post()
  {
    $headers = $this->input->request_headers();    
    $user_info= $this->session->get_userdata('usersessiondata');
    $usertoken=$user_info['usersessiondata']['token'];
    $checkuserauth= $this->user_model->checkuser_token($usertoken);
    $userid=$user_info['usersessiondata']['id'];
    $fileId = $this->security->xss_clean($this->input->post("fileId"));
    $this->form_validation->set_rules("fileId", "fileId", "integer|required");

    if($this->form_validation->run() === FALSE){
      $message = array(
        'status' => false,
        'message' => validation_errors()
      );
      $this->response($message, REST_Controller::HTTP_NOT_FOUND);
    }else{
      if(empty(!$checkuserauth))
      {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('avtar')) {
          $error = array('error' => $this->upload->display_errors());
          $returndata = array('status'=>404,'data'=>$error,'message'=>'image upload failed');
        }
        else{
          $filePath='';
          $data = array('upload_data' => $this->upload->data());
          $filePath=$data['upload_data']['full_path'];
          $file_type=$data['upload_data']['file_type'];
          $uploadData = array("filepath" => $filePath,"filetype" => $file_type, "updatedBy"=>$userid, "updateOn"=>date('Y-m-d H:i:s'));
          $checkuserauth= $this->user_model->updateimage($fileId,$userid,$uploadData);
          if($checkuserauth){
            $message = [
              'status' => true,
              'data' => $uploadData,
              'message' => "image uploaded successfully",
              ];
           $this->response($message, REST_Controller::HTTP_OK);
           }else{
              $message = [
                'status' => false,
                'message' => "failed to update image",
              ];
              $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
      }else{
        $message = [
          'status' => FALSE,
          'message' => "Your not authorize to add data",
        ];
        $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
      } 
    } 

 }

  public function index_get()
    {
      $user_info= $this->session->get_userdata('usersessiondata');
       if(!empty($user_info['usersessiondata']))
       {
            // echo "<pre>"; print_r($all_image);exit();
       $userrole=$user_info['usersessiondata']['role'];
       $usertoken=$user_info['usersessiondata']['token'];
       $sessionuser=$user_info['usersessiondata']['id'];


       $checkuserauth= $this->user_model->checkuser_token($usertoken);
       if(empty(!$checkuserauth))
       {
          $checkrole= $this->user_model->checkuser_role($userrole);
          // print_r($checkrole[0]->role);exit();
          if($checkrole[0]->role != "User")
          {
            $all_file= $this->File_model->get_all_file();
            if($all_file)
            {
               $message = [
                    'status' => FALSE,
                    'Data'=>$all_file,
                    'message' => "All Data",
                                   ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
            }

         }else{

            $user_file= $this->File_model->get_user_file($sessionuser);
             // echo "<pre>"; print_r($user_file);exit();
            if($user_file)
            {

           $message = [
                    'status' => FALSE,
                    'Data'=>$user_file,
                    'message' => "All Data",
                                   ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);
             }else{

              $message = [
                    'status' => FALSE,
                    'message' => "file Not available ",
                                   ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
             }   
         }

       }else{
                $message = [
                    'status' => FALSE,
                    'message' => "unauthorize user",
                                   ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

       }
     }else{
      $message = [
                    'status' => FALSE,
                    'message' => "unauthorize user",
                                   ];
                $this->response($message, REST_Controller::HTTP_UNAUTHORIZED);

     }
     } 

 
}
 ?>
